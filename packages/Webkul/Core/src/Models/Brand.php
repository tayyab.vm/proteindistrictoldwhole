<?php

namespace Webkul\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Core\Contracts\Brand as BrandContract;

class Brand extends Model implements BrandContract
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'brand';
    protected $fillable = [
        'code', 'name','name_ar'
    ];

    /**
     * Set brand code in capital
     *
     * @param  string  $value
     * @return void
     */
    public function setCodeAttribute($code)
    {
        $this->attributes['code'] = strtoupper($code);
    }

    /**
     * Get the brand_exchange associated with the brand.
     */

}