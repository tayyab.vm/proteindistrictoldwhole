<?php

namespace Webkul\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Core\Contracts\Dtype as DtypeContract;

class Dtype extends Model implements DtypeContract
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'dtype';
    protected $fillable = [
        'code', 'name','name_ar'
    ];

    /**
     * Set dtype code in capital
     *
     * @param  string  $value
     * @return void
     */
    public function setCodeAttribute($code)
    {
        $this->attributes['code'] = strtoupper($code);
    }

    /**
     * Get the dtype_exchange associated with the dtype.
     */

}