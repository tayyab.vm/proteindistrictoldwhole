<?php

namespace Webkul\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Core\Contracts\Goal as GoalContract;

class Goal extends Model implements GoalContract
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'goal';
    protected $fillable = [
        'code', 'name','name_ar'
    ];

    /**
     * Set goal code in capital
     *
     * @param  string  $value
     * @return void
     */
    public function setCodeAttribute($code)
    {
        $this->attributes['code'] = strtoupper($code);
    }

    /**
     * Get the goal_exchange associated with the goal.
     */

}