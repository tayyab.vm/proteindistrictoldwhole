<?php

namespace Webkul\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Core\Contracts\Brand as BrandContract;

class ChannelLocale extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'channel_locales';


    /**
     * Set brand code in capital
     *
     * @param  string  $value
     * @return void
     */


    /**
     * Get the brand_exchange associated with the brand.
     */

}