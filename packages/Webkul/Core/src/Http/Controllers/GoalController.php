<?php

namespace Webkul\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Webkul\Core\Repositories\GoalRepository as Goal;

/**
 * Goal controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class GoalController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * GoalRepository object
     *
     * @var array
     */
    protected $goal;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Core\Repositories\GoalRepository $goal
     * @return void
     */
    public function __construct(Goal $goal)
    {
        $this->goal = $goal;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'code' => 'required|min:1|max:5|unique:goal,code',
            'name' => 'required'
        ]);

        Event::fire('core.channel.create.before');

        $goal = $this->goal->create(request()->all());

        Event::fire('core.goal.create.after', $goal);

        session()->flash('success', trans('admin::app.settings.goals.create-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $goal = $this->goal->findOrFail($id);

        return view($this->_config['view'], compact('goal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            
            'name' => 'required'
        ]);

        Event::fire('core.goal.update.before', $id);

        $goal = $this->goal->update(request()->all(), $id);

        Event::fire('core.goal.update.after', $goal);

        session()->flash('success', trans('admin::app.settings.goals.update-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $goal = $this->goal->findOrFail($id);

        if ($this->goal->count() == 1) {
            session()->flash('warning', trans('admin::app.settings.goals.last-delete-error'));
        } else {
            try {
                Event::fire('core.goal.delete.before', $id);

                $this->goal->delete($id);

                Event::fire('core.goal.delete.after', $id);

                session()->flash('success', trans('admin::app.settings.goals.delete-success'));

                return response()->json(['message' => true], 200);
            } catch (\Exception $e) {
                session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Goal']));
            }
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy() {
        $suppressFlash = false;

        if (request()->isMethod('post')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    Event::fire('core.goal.delete.before', $value);

                    $this->goal->delete($value);

                    Event::fire('core.goal.delete.after', $value);
                } catch(\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', ['resource' => 'goal']));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'goal']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }
}