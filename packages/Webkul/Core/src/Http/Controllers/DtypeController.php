<?php

namespace Webkul\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Webkul\Core\Repositories\DtypeRepository as Dtype;

/**
 * Dtype controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DtypeController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * DtypeRepository object
     *
     * @var array
     */
    protected $dtype;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Core\Repositories\DtypeRepository $dtype
     * @return void
     */
    public function __construct(Dtype $dtype)
    {
        $this->dtype = $dtype;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'code' => 'required|min:1|max:5|unique:dtype,code',
            'name' => 'required'
        ]);

        Event::fire('core.channel.create.before');

        $dtype = $this->dtype->create(request()->all());

        Event::fire('core.dtype.create.after', $dtype);

        session()->flash('success', trans('admin::app.settings.dtypes.create-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dtype = $this->dtype->findOrFail($id);

        return view($this->_config['view'], compact('dtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            
            'name' => 'required'
        ]);

        Event::fire('core.dtype.update.before', $id);

        $dtype = $this->dtype->update(request()->all(), $id);

        Event::fire('core.dtype.update.after', $dtype);

        session()->flash('success', trans('admin::app.settings.dtypes.update-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dtype = $this->dtype->findOrFail($id);

        if ($this->dtype->count() == 1) {
            session()->flash('warning', trans('admin::app.settings.dtypes.last-delete-error'));
        } else {
            try {
                Event::fire('core.dtype.delete.before', $id);

                $this->dtype->delete($id);

                Event::fire('core.dtype.delete.after', $id);

                session()->flash('success', trans('admin::app.settings.dtypes.delete-success'));

                return response()->json(['message' => true], 200);
            } catch (\Exception $e) {
                session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Dtype']));
            }
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy() {
        $suppressFlash = false;

        if (request()->isMethod('post')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    Event::fire('core.dtype.delete.before', $value);

                    $this->dtype->delete($value);

                    Event::fire('core.dtype.delete.after', $value);
                } catch(\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', ['resource' => 'dtype']));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'dtype']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }
}