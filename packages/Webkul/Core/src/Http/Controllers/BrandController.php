<?php

namespace Webkul\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Webkul\Core\Repositories\BrandRepository as Brand;

/**
 * Brand controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class BrandController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * BrandRepository object
     *
     * @var array
     */
    protected $brand;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Core\Repositories\BrandRepository $brand
     * @return void
     */
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'code' => 'required|min:1|max:5|unique:brand,code',
            'name' => 'required'
        ]);

        Event::fire('core.channel.create.before');

        $brand = $this->brand->create(request()->all());

        Event::fire('core.brand.create.after', $brand);

        session()->flash('success', trans('admin::app.settings.brands.create-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = $this->brand->findOrFail($id);

        return view($this->_config['view'], compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            
            'name' => 'required'
        ]);

        Event::fire('core.brand.update.before', $id);

        $brand = $this->brand->update(request()->all(), $id);

        Event::fire('core.brand.update.after', $brand);

        session()->flash('success', trans('admin::app.settings.brands.update-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = $this->brand->findOrFail($id);

        if ($this->brand->count() == 1) {
            session()->flash('warning', trans('admin::app.settings.brands.last-delete-error'));
        } else {
            try {
                Event::fire('core.brand.delete.before', $id);

                $this->brand->delete($id);

                Event::fire('core.brand.delete.after', $id);

                session()->flash('success', trans('admin::app.settings.brands.delete-success'));

                return response()->json(['message' => true], 200);
            } catch (\Exception $e) {
                session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Brand']));
            }
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy() {
        $suppressFlash = false;

        if (request()->isMethod('post')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    Event::fire('core.brand.delete.before', $value);

                    $this->brand->delete($value);

                    Event::fire('core.brand.delete.after', $value);
                } catch(\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', ['resource' => 'brand']));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'brand']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }
}