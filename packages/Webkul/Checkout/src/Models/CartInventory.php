<?php

namespace Webkul\Checkout\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Checkout\Contracts\CartInventory as CartInventoryContract;

class CartInventory extends Model implements CartInventoryContract
{
    protected $table = 'cart_inventory';
}