<?php

namespace Webkul\Attribute\Models;

use Illuminate\Database\Eloquent\Model;



class DType extends Model
{
    protected $table = 'dtype';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['value'];
    /**
     * Get all of the attributes for the attribute groups.
     */

    public static function getlist()
    {
        $result= self::where("active", 1)->get();
        return $result;
    }
    public static function getName($id)
    {
        try {
            $result = self::find($id)->name;
            return $result;
        }
        catch(\Exception $ex)
        {
            return "No Type";
        }
    }
    /**
     * Get all of the attributes for the attribute groups.
     */

}