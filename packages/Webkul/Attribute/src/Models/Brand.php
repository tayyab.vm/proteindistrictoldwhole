<?php

namespace Webkul\Attribute\Models;

use Illuminate\Database\Eloquent\Model;



class Brand extends Model
{
    protected $table = 'brand';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['value'];
    /**
     * Get all of the attributes for the attribute groups.
     */

    public static function getlist()
    {
        $result= self::where("active", "1")->get();
        return $result;
    }
    public static function getName($id)
    {
        $result= self::find($id)->name;
        return $result;
    }
    /**
     * Get all of the attributes for the attribute groups.
     */

}