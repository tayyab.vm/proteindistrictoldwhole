<?php

namespace Webkul\Product\Repositories;

use DB;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Event;
use Storage;
use Webkul\API\Http\Resources\Catalog\Product2 as ProductResource2;
use Webkul\Attribute\Repositories\AttributeOptionRepository;
use Webkul\Attribute\Repositories\AttributeRepository;
use Webkul\Core\Eloquent\Repository;
use Webkul\Product\Models\ProductAttributeValue;

/**
 * Product Repository
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductRepository extends Repository
{
    /**
     * AttributeRepository object
     *
     * @var array
     */
    protected $attribute;

    /**
     * AttributeOptionRepository object
     *
     * @var array
     */
    protected $attributeOption;

    /**
     * ProductAttributeValueRepository object
     *
     * @var array
     */
    protected $attributeValue;

    /**
     * ProductFlatRepository object
     *
     * @var array
     */
    protected $productInventory;

    /**
     * ProductImageRepository object
     *
     * @var array
     */
    protected $productImage;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Attribute\Repositories\AttributeRepository $attribute
     * @param  Webkul\Attribute\Repositories\AttributeOptionRepository $attributeOption
     * @param  Webkul\Attribute\Repositories\ProductAttributeValueRepository $attributeValue
     * @param  Webkul\Product\Repositories\ProductInventoryRepository $productInventory
     * @param  Webkul\Product\Repositories\ProductImageRepository $productImage
     * @return void
     */
    public function __construct(
        AttributeRepository $attribute,
        AttributeOptionRepository $attributeOption,
        ProductAttributeValueRepository $attributeValue,
        ProductInventoryRepository $productInventory,
        ProductImageRepository $productImage,
        App $app)
    {
        $this->attribute = $attribute;

        $this->attributeOption = $attributeOption;

        $this->attributeValue = $attributeValue;

        $this->productInventory = $productInventory;

        $this->productImage = $productImage;

        parent::__construct($app);
    }

    /**->where('product_flat.visible_individually', 1)
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\Product\Contracts\Product';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        //before store of the product
        Event::fire('catalog.product.create.before');

        $product = $this->model->create($data);

        $nameAttribute = $this->attribute->findOneByField('code', 'status');
        $this->attributeValue->create([
            'product_id' => $product->id,
            'attribute_id' => $nameAttribute->id,
            'value' => 1
        ]);

        if (isset($data['super_attributes'])) {

            $super_attributes = [];

            foreach ($data['super_attributes'] as $attributeCode => $attributeOptions) {
                $attribute = $this->attribute->findOneByField('code', $attributeCode);

                $super_attributes[$attribute->id] = $attributeOptions;

                $product->super_attributes()->attach($attribute->id);
            }

            foreach (array_permutation($super_attributes) as $permutation) {
                $this->createVariant($product, $permutation);
            }
        }

        //after store of the product
        Event::fire('catalog.product.create.after', $product);

        return $product;
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        Event::fire('catalog.product.update.before', $id);

        $product = $this->find($id);

        if ($product->parent_id && $this->checkVariantOptionAvailabiliy($data, $product)) {
            $data['parent_id'] = NULL;
        }
        try {
            if (is_null($data['points']) || $data['points'] == 0 || $data['price'] == 0) {
                $data['points'] = $data['price'] * core()->getCurrentCurrencyPoint();
            }
        } catch (\Exception $ex) {
            $data['points'] = 0;
        }

        $product->update($data);

        $attributes = $product->attribute_family->custom_attributes;

        foreach ($attributes as $attribute) {
            if (!isset($data[$attribute->code]) || (in_array($attribute->type, ['date', 'datetime']) && !$data[$attribute->code]))
                continue;

            if ($attribute->type == 'multiselect') {
                $data[$attribute->code] = implode(",", $data[$attribute->code]);
            }

            if ($attribute->type == 'image' || $attribute->type == 'file') {
                $dir = 'product';
                if (gettype($data[$attribute->code]) == 'object') {
                    $data[$attribute->code] = request()->file($attribute->code)->store($dir);
                } else {
                    $data[$attribute->code] = NULL;
                }
            }

            $attributeValue = $this->attributeValue->findOneWhere([
                'product_id' => $product->id,
                'attribute_id' => $attribute->id,
                'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                'locale' => $attribute->value_per_locale ? $data['locale'] : null
            ]);

            if (!$attributeValue) {
                $this->attributeValue->create([
                    'product_id' => $product->id,
                    'attribute_id' => $attribute->id,
                    'value' => $data[$attribute->code],
                    'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                    'locale' => $attribute->value_per_locale ? $data['locale'] : null
                ]);
            } else {
                $this->attributeValue->update([
                    ProductAttributeValue::$attributeTypeFields[$attribute->type] => $data[$attribute->code]
                ], $attributeValue->id
                );

                if ($attribute->type == 'image' || $attribute->type == 'file') {
                    Storage::delete($attributeValue->text_value);
                }
            }
        }

        if (request()->route()->getName() != 'admin.catalog.products.massupdate') {
            if (isset($data['categories'])) {
                $product->categories()->sync($data['categories']);
            }

            if (isset($data['up_sell'])) {
                $product->up_sells()->sync($data['up_sell']);
            } else {
                $data['up_sell'] = [];
                $product->up_sells()->sync($data['up_sell']);
            }

            if (isset($data['cross_sell'])) {
                $product->cross_sells()->sync($data['cross_sell']);
            } else {
                $data['cross_sell'] = [];
                $product->cross_sells()->sync($data['cross_sell']);
            }

            if (isset($data['related_products'])) {
                $product->related_products()->sync($data['related_products']);
            } else {
                $data['related_products'] = [];
                $product->related_products()->sync($data['related_products']);
            }

            $previousVariantIds = $product->variants->pluck('id');

            if (isset($data['variants'])) {
                foreach ($data['variants'] as $variantId => $variantData) {
                    if (str_contains($variantId, 'variant_')) {
                        $permutation = [];
                        foreach ($product->super_attributes as $superAttribute) {
                            $permutation[$superAttribute->id] = $variantData[$superAttribute->code];
                        }

                        $this->createVariant($product, $permutation, $variantData);
                    } else {
                        if (is_numeric($index = $previousVariantIds->search($variantId))) {
                            $previousVariantIds->forget($index);
                        }

                        $variantData['channel'] = $data['channel'];
                        $variantData['locale'] = $data['locale'];

                        $this->updateVariant($variantData, $variantId);
                    }
                }
            }

            foreach ($previousVariantIds as $variantId) {
                $this->delete($variantId);
            }

            $this->productInventory->saveInventories($data, $product);

            $this->productImage->uploadImages($data, $product);
        }

        Event::fire('catalog.product.update.after', $product);

        return $product;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        Event::fire('catalog.product.delete.before', $id);

        parent::delete($id);

        Event::fire('catalog.product.delete.after', $id);
    }

    /**
     * @param mixed $product
     * @param array $permutation
     * @param array $data
     * @return mixed
     */
    public function createVariant($product, $permutation, $data = [])
    {
        if (!count($data)) {
            $data = [
                "sku" => $product->sku . '-variant-' . implode('-', $permutation),
                "name" => "",
                "inventories" => [],
                "price" => 0,
                "weight" => 0,
                "points" => 0,
                "status" => 1
            ];
        }

        try {
            if (is_null($data['points']) || $data['points'] == 0 || $data['price'] == 0) {
                $data['points'] = $data['price'] * core()->getCurrentCurrencyPoint();
            }
        } catch (\Exception $ex) {
            $data['points'] = 0;
        }

        $variant = $this->model->create([
            'parent_id' => $product->id,
            'type' => 'simple',
            'attribute_family_id' => $product->attribute_family_id,
            'sku' => $data['sku'],
            'points' => $data['points'],
        ]);

        foreach (['sku', 'name', 'price', 'points', 'weight', 'status'] as $attributeCode) {
            $attribute = $this->attribute->findOneByField('code', $attributeCode);

            if ($attribute->value_per_channel) {
                if ($attribute->value_per_locale) {
                    foreach (core()->getAllChannels() as $channel) {
                        foreach (core()->getAllLocales() as $locale) {
                            $this->attributeValue->create([
                                'product_id' => $variant->id,
                                'attribute_id' => $attribute->id,
                                'channel' => $channel->code,
                                'locale' => $locale->code,
                                'value' => $data[$attributeCode]
                            ]);
                        }
                    }
                } else {
                    foreach (core()->getAllChannels() as $channel) {
                        $this->attributeValue->create([
                            'product_id' => $variant->id,
                            'attribute_id' => $attribute->id,
                            'channel' => $channel->code,
                            'value' => $data[$attributeCode]
                        ]);
                    }
                }
            } else {
                if ($attribute->value_per_locale) {
                    foreach (core()->getAllLocales() as $locale) {
                        $this->attributeValue->create([
                            'product_id' => $variant->id,
                            'attribute_id' => $attribute->id,
                            'locale' => $locale->code,
                            'value' => $data[$attributeCode]
                        ]);
                    }
                } else {
                    $this->attributeValue->create([
                        'product_id' => $variant->id,
                        'attribute_id' => $attribute->id,
                        'value' => $data[$attributeCode]
                    ]);
                }
            }
        }

        foreach ($permutation as $attributeId => $optionId) {
            $this->attributeValue->create([
                'product_id' => $variant->id,
                'attribute_id' => $attributeId,
                'value' => $optionId
            ]);
        }

        $this->productInventory->saveInventories($data, $variant);

        return $variant;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function updateVariant(array $data, $id)
    {
        try {
            if (is_null($data['points']) || $data['points'] == 0 || $data['price'] == 0) {
                $data['points'] = $data['price'] * core()->getCurrentCurrencyPoint();
            }
        } catch (\Exception $ex) {
            $data['points'] = 0;
        }
        $variant = $this->find($id);

        $variant->update(['sku' => $data['sku'], 'points' => $data['points']]);

        foreach (['sku', 'name', 'price', 'points', 'weight', 'status'] as $attributeCode) {
            $attribute = $this->attribute->findOneByField('code', $attributeCode);

            $attributeValue = $this->attributeValue->findOneWhere([
                'product_id' => $id,
                'attribute_id' => $attribute->id,
                'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                'locale' => $attribute->value_per_locale ? $data['locale'] : null
            ]);

            if (!$attributeValue) {
                $this->attributeValue->create([
                    'product_id' => $id,
                    'attribute_id' => $attribute->id,
                    'value' => $data[$attribute->code],
                    'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                    'locale' => $attribute->value_per_locale ? $data['locale'] : null
                ]);
            } else {
                $this->attributeValue->update([
                    ProductAttributeValue::$attributeTypeFields[$attribute->type] => $data[$attribute->code]
                ], $attributeValue->id);
            }
        }

        $this->productInventory->saveInventories($data, $variant);

        return $variant;
    }

    /**
     * @param array $data
     * @param mixed $product
     * @return mixed
     */
    public function checkVariantOptionAvailabiliy($data, $product)
    {
        $parent = $product->parent;

        $superAttributeCodes = $parent->super_attributes->pluck('code');

        $isAlreadyExist = false;

        foreach ($parent->variants2 as $variant) {
            if ($variant->id == $product->id)
                continue;

            $matchCount = 0;

            foreach ($superAttributeCodes as $attributeCode) {
                if (!isset($data[$attributeCode]))
                    return false;

                if ($data[$attributeCode] == $variant->{$attributeCode})
                    $matchCount++;
            }

            if ($matchCount == $superAttributeCodes->count()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param integer $categoryId
     * @return Collection
     */
    public function getAll($categoryId = null)
    {
        $params = request()->input();

        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) use ($params, $categoryId) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.*')
                //->addSelect(DB::raw('(select qty from product_inventories where product_inventories.product_id=product_flat.product_id  limit 1) as qty'))
                ->addSelect(DB::raw('IF( product_flat.special_price_from IS NOT NULL
                            AND product_flat.special_price_to IS NOT NULL , IF( NOW( ) >= product_flat.special_price_from
                            AND NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , IF( product_flat.special_price_from IS NULL , IF( product_flat.special_price_to IS NULL , IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , IF( NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) ) , IF( product_flat.special_price_to IS NULL , IF( NOW( ) >= product_flat.special_price_from, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , product_flat.price ) ) ) AS final_price'))
                ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
                ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale);
                // ->where(DB::raw('(select qty from product_inventories where product_inventories.product_id=products.id  and product_inventories.qty>0 limit 1)'),'>', 0);

         /*   $qb->where(function ($query1)  {
                $query1->where(DB::raw('products.type="simple" and (select qty from product_inventories where product_inventories.product_id=products.id  and product_inventories.qty>0 limit 1)'),'>', 0);
                $query1->orwhere(DB::raw('products.type="configurable" and (select qty from product_inventories where product_inventories.product_id in (select tt.id from products tt where parent_id=products.id)  and product_inventories.qty>0 limit 1)'),'>', 0);

            });*/

            if ($categoryId) {
                $qb->where('product_categories.category_id', $categoryId);
            }

            if (is_null(request()->input('status'))) {
                $qb->where('product_flat.status', 1);
            }

            if (is_null(request()->input('visible_individually'))) {
                $qb->where('product_flat.visible_individually', 1);
            }

            $queryBuilder = $qb->leftJoin('product_flat as flat_variants', function ($qb) use ($channel, $locale) {
                $qb->on('product_flat.id', '=', 'flat_variants.parent_id')
                    ->where('flat_variants.channel', $channel)
                    ->where('flat_variants.locale', $locale);
            });

            if (isset($params['search'])) {
                $qb->where('product_flat.name', 'like', '%' . urldecode($params['search']) . '%');
            }

            if (isset($params['sort'])) {
                $attribute = $this->attribute->findOneByField('code', $params['sort']);

                if ($params['sort'] == 'price') {
                    if ($attribute->code == 'price') {
                        $qb->orderBy('final_price', $params['order']);
                    } else {
                        $qb->orderBy($attribute->code, $params['order']);
                    }
                } else {
                    $qb->orderBy($params['sort'] == 'created_at' ? 'product_flat.created_at' : $attribute->code, $params['order']);
                }
            }

            $qb = $qb->where(function ($query1) {
                foreach (['product_flat', 'flat_variants'] as $alias) {
                    $query1 = $query1->orWhere(function ($query2) use ($alias) {
                        $attributes = $this->attribute->getProductDefaultAttributes(array_keys(request()->input()));

                        foreach ($attributes as $attribute) {
                            $column = $alias . '.' . $attribute->code;

                            $queryParams = explode(',', request()->get($attribute->code));

                            if ($attribute->type != 'price') {
                                $query2 = $query2->where(function ($query3) use ($column, $queryParams) {
                                    foreach ($queryParams as $filterValue) {
                                        $query3 = $query3->orwhereRaw("find_in_set($filterValue, $column)");
                                    }
                                });
                            } else {
                                if ($attribute->code != 'price') {
                                    $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                } else {
                                    $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                }
                            }
                        }
                    });
                }
            });

            return $qb->groupBy('product_flat.id');
        })->paginate(isset($params['limit']) ? $params['limit'] : 9);

        return $results;
    }

    public function getAll2($categoryId = null, $brand = null, $goal = null, $dtype = null)
    {
        $params = request()->input();

        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) use ($params, $categoryId, $brand, $goal, $dtype) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.*')
              // ->addSelect(DB::raw('(select qty from product_inventories where product_inventories.product_id=product_flat.product_id  limit 1) as qty'))
                ->addSelect('brand.name as brand', 'dtype.name as dtype')
                ->addSelect(DB::raw('(select "AED") as currency'))
                ->addSelect(DB::raw('(select CONCAT("' . env('APP_URL') . '/storage",product_images.path)  from product_images where product_images.product_id=product_flat.product_id limit 1) as image'))
                ->addSelect(DB::raw('IF( product_flat.special_price_from IS NOT NULL
                            AND product_flat.special_price_to IS NOT NULL , IF( NOW( ) >= product_flat.special_price_from
                            AND NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , IF( product_flat.special_price_from IS NULL , IF( product_flat.special_price_to IS NULL , IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , IF( NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) ) , IF( product_flat.special_price_to IS NULL , IF( NOW( ) >= product_flat.special_price_from, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , product_flat.price ) ) ) AS final_price'))
                ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
                ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
                ->leftJoin('brand', 'products.brand', '=', 'brand.id')
                ->leftJoin('dtype', 'products.dtype', '=', 'dtype.id')
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale);
                // ->where(DB::raw('(select qty from product_inventories where product_inventories.product_id=products.id and product_inventories.qty>0 limit 1)'),'>=', 0);

            $qb->where(function ($query1)  {
                $query1->where(DB::raw('products.type="simple" and (select qty from product_inventories where product_inventories.product_id=products.id  and product_inventories.qty>0 limit 1)'),'>', 0);
                $query1->orwhere(DB::raw('products.type="configurable" and (select qty from product_inventories where product_inventories.product_id in (select tt.id from products tt where parent_id=products.id)  and product_inventories.qty>0 limit 1)'),'>', 0);

            });
            if ($categoryId) {
                $qb->where('product_categories.category_id', $categoryId);
            }
            if (strlen($brand) > 0) {
                $qb->where('products.brand', $brand);
            }
            if (strlen($dtype) > 0) {
                $qb->where('products.dtype', $dtype);
            }
            if (strlen($goal) > 0) {

                $qb->where(function ($query1) use ($goal) {
                    $query1->where('products.goal', 'like', '%' . ',' . $goal . ',' . '%')
                        ->orwhere('products.goal', 'like', '%' . $goal . ',' . '%')
                        ->orwhere('products.goal', 'like', '%' . ',' . $goal . '%')
                        ->orwhere('products.goal', $goal);
                });

            }

            if (is_null(request()->input('status'))) {
                $qb->where('product_flat.status', 1);
            }
            if (request()->has('deal')) {
                
                // $qb->where(function ($query1) {
                //     $query1->where('product_flat.buyx', '>', 0)
                //         ->orwhere('product_flat.special_price', '>', 0);
                // });
                // $qb->where(function ($query1) {
                //     $query1->where('product_flat.special_price_from', '<', DB::raw('Now()'))
                //         ->where('product_flat.special_price_to', '>', DB::raw('Now()'))
                //         ->orwhereNull('product_flat.special_price_to')->orwhereNull('product_flat.special_price_from');;
                // });
                // $qb->where('products.type', 'simple');

                $qb->whereHas('promotion');
                \Log::debug('promotion',['$citem->product_id']);

                // $qb->groupBy('product_flat.id');
                // dd($qb);

                // die('ssas');
            }

            if (is_null(request()->input('visible_individually'))) {
                $qb->where('product_flat.visible_individually', 1);
            }

            $queryBuilder = $qb->leftJoin('product_flat as flat_variants', function ($qb) use ($channel, $locale) {
                $qb->on('product_flat.id', '=', 'flat_variants.parent_id')
                    ->where('flat_variants.channel', $channel)
                    ->where('flat_variants.locale', $locale);
            });

            if (isset($params['search'])) {
                $qb->where('product_flat.name', 'like', '%' . urldecode($params['search']) . '%');
            }
            if (isset($params['brand'])) {
                $qb->where('brand.name', 'like', '%' . urldecode($params['brand']) . '%');
            }


            if (isset($params['sort'])) {
                $attribute = $this->attribute->findOneByField('code', $params['sort']);

                if ($params['sort'] == 'price') {
                    if ($attribute->code == 'price') {
                        $qb->orderBy('final_price', $params['order']);
                    } else {
                        $qb->orderBy($attribute->code, $params['order']);
                    }
                } else {
                    $qb->orderBy($params['sort'] == 'created_at' ? 'product_flat.created_at' : $attribute->code, $params['order']);
                }
            }

            $qb = $qb->where(function ($query1) {
                foreach (['product_flat', 'flat_variants'] as $alias) {
                    $query1 = $query1->orWhere(function ($query2) use ($alias) {
                        $attributes = $this->attribute->getProductDefaultAttributes(array_keys(request()->input()));

                        foreach ($attributes as $attribute) {
                            $column = $alias . '.' . $attribute->code;

                            $queryParams = explode(',', request()->get($attribute->code));

                            if ($attribute->type != 'price') {
                                $query2 = $query2->where(function ($query3) use ($column, $queryParams) {
                                    foreach ($queryParams as $filterValue) {
                                        $query3 = $query3->orwhereRaw("find_in_set($filterValue, $column)");
                                    }
                                });
                            } else {
                                if ($attribute->code != 'price') {
                                    $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                } else {
                                    $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                }
                            }
                        }
                    });
                }
            });

            return $qb->groupBy('product_flat.id');
        });

        return ProductResource2::collection($results->get());
    }

    public function getTop10($categoryId = null, $brand = null, $goal = null, $dtype = null)
    {
        $params = request()->input();

        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) use ($params, $categoryId, $brand, $goal, $dtype) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.*')
                ->addSelect('brand.name as brand', 'dtype.name as dtype')
             //   ->addSelect(DB::raw('(select qty from product_inventories where product_inventories.product_id=product_flat.product_id   limit 1)as qty'))
                ->addSelect(DB::raw('(select CONCAT("' . env('APP_URL') . '/storage",product_images.path)  from product_images where product_images.product_id=product_flat.product_id limit 1) as image'))
                ->addSelect(DB::raw('IF( product_flat.special_price_from IS NOT NULL
                            AND product_flat.special_price_to IS NOT NULL , IF( NOW( ) >= product_flat.special_price_from
                            AND NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , IF( product_flat.special_price_from IS NULL , IF( product_flat.special_price_to IS NULL , IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , IF( NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) ) , IF( product_flat.special_price_to IS NULL , IF( NOW( ) >= product_flat.special_price_from, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , product_flat.price ) ) ) AS final_price'))
                ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
                ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
                ->leftJoin('dtype', 'products.dtype', '=', 'dtype.id')
                /*  ->leftJoin('product_images', 'product_flat.id', '=', 'product_images.product_id')*/
                ->leftJoin('brand', 'products.brand', '=', 'brand.id')
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale);
               //

            $qb->where(function ($query1)  {
                $query1->where(DB::raw('products.type="simple" and (select qty from product_inventories where product_inventories.product_id=products.id  and product_inventories.qty>0 limit 1)'),'>', 0);
                $query1->orwhere(DB::raw('products.type="configurable" and (select qty from product_inventories where product_inventories.product_id in (select tt.id from products tt where parent_id=products.id)  and product_inventories.qty>0 limit 1)'),'>', 0);

            });

            if ($categoryId) {
                $qb->where('product_categories.category_id', $categoryId);
            }
            if (strlen($brand) > 0) {
                $qb->where('products.brand', $brand);
            }
            if (strlen($dtype) > 0) {
                $qb->where('products.dtype', $dtype);
            }
            if (strlen($goal) > 0) {

                $qb->where(function ($query1) use ($goal) {
                    $query1->where('products.goal', 'like', '%' . ',' . $goal . ',' . '%')
                        ->orwhere('products.goal', 'like', '%' . $goal . ',' . '%')
                        ->orwhere('products.goal', 'like', '%' . ',' . $goal . '%')
                        ->orwhere('products.goal', $goal);
                });

            }


            if (is_null(request()->input('status'))) {
                $qb->where('product_flat.status', 1);
            }

            /*  if (is_null(request()->input('visible_individually'))) {
                  $qb->where('product_flat.visible_individually', 1);
              }*/

            $queryBuilder = $qb->leftJoin('product_flat as flat_variants', function ($qb) use ($channel, $locale) {
                $qb->on('product_flat.id', '=', 'flat_variants.parent_id')
                    ->where('flat_variants.channel', $channel)
                    ->where('flat_variants.locale', $locale);
            });

            if (isset($params['search'])) {
                $qb->where('product_flat.name', 'like', '%' . urldecode($params['search']) . '%');
            }
            if (isset($params['brand'])) {
                $qb->where('brand.name', 'like', '%' . urldecode($params['brand']) . '%');
            }


            if (isset($params['sort'])) {
                $attribute = $this->attribute->findOneByField('code', $params['sort']);

                if ($params['sort'] == 'price') {
                    if ($attribute->code == 'price') {
                        $qb->orderBy('final_price', $params['order']);
                    } else {
                        $qb->orderBy($attribute->code, $params['order']);
                    }
                } else {
                    $qb->orderBy('product_flat.created_at', 'desc');
                }
            }

            $qb = $qb->where(function ($query1) {
                foreach (['product_flat', 'flat_variants'] as $alias) {
                    $query1 = $query1->orWhere(function ($query2) use ($alias) {
                        $attributes = $this->attribute->getProductDefaultAttributes(array_keys(request()->input()));

                        foreach ($attributes as $attribute) {
                            $column = $alias . '.' . $attribute->code;

                            $queryParams = explode(',', request()->get($attribute->code));

                            if ($attribute->type != 'price') {
                                $query2 = $query2->where(function ($query3) use ($column, $queryParams) {
                                    foreach ($queryParams as $filterValue) {
                                        $query3 = $query3->orwhereRaw("find_in_set($filterValue, $column)");
                                    }
                                });
                            } else {
                                if ($attribute->code != 'price') {
                                    $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                } else {
                                    $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                }
                            }
                        }
                    });
                }
            });
            $qb->whereNull('product_flat.parent_id');
            $qb->orderby('product_flat.id', 'desc');
            return $qb->groupBy('product_flat.id')->take(10);
        });

        return ProductResource2::collection($results->get());
    }

    /**
     * Retrive product from slug
     *
     * @param string $slug
     * @return mixed
     */
    public function findBySlugOrFail($slug, $columns = null)
    {
        $product = app('Webkul\Product\Repositories\ProductFlatRepository')->findOneWhere([
            'url_key' => $slug,
            'locale' => app()->getLocale(),
            'channel' => core()->getCurrentChannelCode(),
        ]);

        if (!$product) {
            throw (new ModelNotFoundException)->setModel(
                get_class($this->model), $slug
            );
        }

        return $product;
    }

    /**
     * Returns newly added product
     *
     * @return Collection
     */
    public function getNewProducts()
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                ->addSelect('product_flat.*')
                ->where('product_flat.status', 1)
                ->where('product_flat.visible_individually', 1)
                ->where('product_flat.new', 1)
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->orderBy('product_id', 'desc');
        })->paginate(4);

        return $results;
    }

    /**
     * Returns featured product
     *
     * @return Collection
     */
    public function getFeaturedProducts()
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                ->addSelect('product_flat.*')
                ->where('product_flat.status', 1)
                ->where('product_flat.visible_individually', 1)
                ->where('product_flat.featured', 1)
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->orderBy('product_id', 'desc');
        })->paginate(4);

        return $results;
    }

    /**
     * Search Product by Attribute
     *
     * @return Collection
     */
    public function searchProductByAttribute($term)
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function ($query) use ($term) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                ->addSelect('product_flat.*')
                ->where('product_flat.status', 1)
                ->where('product_flat.visible_individually', 1)
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->whereNotNull('product_flat.url_key')
                ->where('product_flat.name', 'like', '%' . urldecode($term) . '%')
                ->orderBy('product_id', 'desc');
        })->paginate(16);

        return $results;
    }

    /**
     * Returns product's super attribute with options
     *
     * @param Product $product
     * @return Collection
     */
    public function getSuperAttributes($product)
    {
        $superAttrbutes = [];

        foreach ($product->super_attributes as $key => $attribute) {
            $superAttrbutes[$key] = $attribute->toArray();

            foreach ($attribute->options as $option) {
                $superAttrbutes[$key]['options'][] = [
                    'id' => $option->id,
                    'admin_name' => $option->admin_name,
                    'sort_order' => $option->sort_order,
                    'swatch_value' => $option->swatch_value,
                ];
            }
        }

        return $superAttrbutes;
    }
}