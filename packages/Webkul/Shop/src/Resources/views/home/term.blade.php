@extends('shop::layouts.master')

@section('page_title')
    Welcome to Protein District
@endsection

@section('content-wrapper')

    {!! view_render_event('bagisto.shop.home.content.before') !!}
    <div class="row">
    <div class="col-8 col-lg-offset-2">
<div class="text-justify">
    {!! $term  !!}
</div>
    </div>
    </div>
    {{ view_render_event('bagisto.shop.home.content.after') }}

@endsection
