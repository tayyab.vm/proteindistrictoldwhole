{!! view_render_event('bagisto.shop.products.price.before', ['product' => $product]) !!}

<div class="product-price">
    @inject ('priceHelper', 'Webkul\Product\Helpers\Price')

    @if ($product->type == 'configurable')
        <span class="price-label">{{ __('shop::app.products.price-label') }}</span>

        <span class="final-price">{{ core()->currency($priceHelper->getMinimalPrice($product)) }}</span>
    @else
        @if ($priceHelper->haveXYSpecialPrice($product,$product->buyx))
            <div class="sticker buyxgety">
                Buy {{$priceHelper->getxy( $product->buyx) }} get {{ $priceHelper->getxy( $product->gety) }} Free
            </div>
            <span class="max-qty">Maximum Quantity For Free {{ $product->maxdiscount }}</span>
        @endif

        @if ($priceHelper->haveSpecialPrice($product) )
            <div class="sticker sale">
                @if (!is_null( $product->buyx)&& $product->buyx>0 && ($product->gety==0 || is_null( $product->gety)))
                    Buy {{$product->buyx}} get {{number_format(($product->special_price/$product->price)*100) }}% OFF
                @else
                    {{ __('shop::app.products.sale') }}
                @endif

            </div>

            @if ( is_null( $product->buyx)|| $product->buyx==0)
                <span class="regular-price">{{ core()->currency($product->price) }}</span>

                <span class="special-price">{{ core()->currency($priceHelper->getSpecialPrice($product)) }}</span>
            @else
                <span>{{ core()->currency($product->price) }}</span>
            @endif
        @else
            <span>{{ core()->currency($product->price) }}</span>
        @endif
    @endif
</div>

{!! view_render_event('bagisto.shop.products.price.after', ['product' => $product]) !!}