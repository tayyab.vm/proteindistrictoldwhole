@component('shop::emails.layouts.master')
    {{-- <div style="text-align: center;">
        <a href="{{ config('app.url') }}">
            <img src="{{ bagisto_asset('images/logo.svg') }}">
        </a>
    </div> --}}

    <div style="padding: 30px;">
        <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
            <span style="font-weight: bold;">
                {{-- {{ __('shop::app.mail.order.heading') }} --}}
                <h3 style="color:#00D149">Order # {{$order->id}} ({{$order->created_at}})</h3>
            </span>

            {{-- <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {{ __('shop::app.mail.order.dear', ['customer_name' => $order->customer_full_name]) }},
            </p>

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {!! __('shop::app.mail.order.greeting', [
                    'order_id' => '<a href="' . route('customer.orders.view', $order->id) . '" style="color: #0041FF; font-weight: bold;">#' . $order->id . '</a>',
                    'created_at' => $order->created_at
                    ])
                !!}
            </p> --}}
        </div>

        <div class="section-content">
            <div class="table mb-20">
                <table style="overflow-x: auto; border-collapse: collapse;
                border-spacing: 0;width: 100%">
                    <thead>
                        <tr style="background-color: #f2f2f2">
                            {{-- <th style="text-align: left;padding: 8px">{{ __('shop::app.customer.account.order.view.SKU') }}</th> --}}
                            <th style="text-align: left;padding: 8px;border:1px solid lightgrey">Product</th>
                            <th style="text-align: left;padding: 8px;border:1px solid lightgrey">Quantity</th>
                            <th style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ __('shop::app.customer.account.order.view.price') }}</th>
                            
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($order->items as $item)
                            <tr>
                                {{-- <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}" style="text-align: left;padding: 8px">{{ $item->child ? $item->child->sku : $item->sku }}</td> --}}

                                <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}" style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ $item->name }}
                                <br>
                                    @if ($html = $item->getOptionDetailHtml())
                                    <div style="">
                                        <label style="margin-top: 10px;color: #5E5E5E; display: block;">
                                            {{ $html }}
                                        </label>
                                    </div>
                                    @endif
                                </td>

                                <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}" style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ $item->qty_ordered }}</td>
                                
                                <td data-value="{{ __('shop::app.customer.account.order.view.price') }}" style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ core()->formatPrice($item->price, $order->order_currency_code) }}
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey" colspan="2"><b>Subtotal:</b></td>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}</td>
                            {{-- <div style="font-size: 16px;color: #242424;line-height: 30px;float: right;width: 40%;margin-top: 20px;">
                                <div>
                                    <span>{{ __('shop::app.mail.order.subtotal') }}</span>
                                    <span style="float: right;">
                                        {{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}
                                    </span>
                                </div>
                    
                                <div>
                                    <span>{{ __('shop::app.mail.order.shipping-handling') }}</span>
                                    <span style="float: right;">
                                        {{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}
                                    </span>
                                </div>
                    
                                <div>
                                    <span>{{ __('shop::app.mail.order.tax') }}</span>
                                    <span style="float: right;">
                                        {{ core()->formatPrice($order->tax_amount, $order->order_currency_code) }}
                                    </span>
                                </div>
                    
                                @if ($order->discount_amount > 0)
                                    <div>
                                        <span>{{ __('shop::app.mail.order.discount') }}</span>
                                        <span style="float: right;">
                                            {{ core()->formatPrice($order->discount_amount, $order->order_currency_code) }}
                                        </span>
                                    </div>
                                @endif
                    
                                <div style="font-weight: bold">
                                    <span>{{ __('shop::app.mail.order.grand-total') }}</span>
                                    <span style="float: right;">
                                        {{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}
                                    </span>
                                </div>
                            </div> --}}
                        </tr>
                        <tr>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey" colspan="2"><b>Shipping:</b></td>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey" colspan="2"><b>Payment Method:</b></td>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ core()->getConfigData('sales.paymentmethods.'.$order->payment->method.'.title') }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey" colspan="2"><b>Total:</b></td>
                            <td style="text-align: left;padding: 8px;border:1px solid lightgrey">{{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {{-- <div style="font-weight: bold;font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 20px !important;">
            {{ __('shop::app.mail.order.summary') }}
        </div> --}}
        <div style="color: #242424;line-height: 30px;margin-bottom: 34px;">
            <span style="font-weight: bold;">
                {{-- {{ __('shop::app.mail.order.heading') }} --}}
                <h3 style="color:#00D149">Customer Details</h3>
            </span>
            <div style="margin-bottom: 40px;">
                <b>Email:</b> {{ $order->shipping_address->email }}
                <br>
                <b>Tel:</b> {{ $order->shipping_address->phone }}
            </div>
        </div>


        <div style="display: flex;flex-direction: row;margin-top: 20px;justify-content: space-between;margin-bottom: 40px;">
            <div style="line-height: 25px;">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    <span style="color:#00D149">{{ __('shop::app.mail.order.shipping-address') }}</span>
                </div>

                <div>
                    {{ $order->shipping_address->name }}
                </div>

                <div>
                    {{ $order->shipping_address->address1 }}, {{ $order->shipping_address->state }}
                </div>

                <div>
                    {{ core()->country_name($order->shipping_address->country) }} {{ $order->shipping_address->postcode }}
                </div>

                {{-- <div>---</div>

                <div style="margin-bottom: 40px;">
                    {{ __('shop::app.mail.order.contact') }} : {{ $order->shipping_address->phone }}
                </div>

                <div style="font-size: 16px;color: #242424; font-weight: bold">
                    {{ __('shop::app.mail.order.shipping') }}
                </div> --}}

                {{-- <div style="font-size: 16px;color: #242424;">
                    {{ $order->shipping_title }}
                </div> --}}
            </div>

            <div style="line-height: 25px;margin-left:30px">
                <div style="font-weight: bold;font-size: 16px;color: #242424;">
                    <span style="color:#00D149">{{ __('shop::app.mail.order.billing-address') }}</span>
                </div>

                <div>
                    {{ $order->billing_address->name }}
                </div>

                <div>
                    {{ $order->billing_address->address1 }}, {{ $order->billing_address->state }}
                </div>

                <div>
                    {{ core()->country_name($order->billing_address->country) }} {{ $order->billing_address->postcode }}
                </div>

                {{-- <div>---</div>

                <div style="margin-bottom: 40px;">
                    {{ __('shop::app.mail.order.contact') }} : {{ $order->billing_address->phone }}
                </div> --}}

                {{-- <div style="font-size: 16px; color: #242424; font-weight: bold">
                    {{ __('shop::app.mail.order.payment') }}
                </div> --}}

                {{-- <div style="font-size: 16px; color: #242424;">
                    {{ core()->getConfigData('sales.paymentmethods.'.$order->payment->method.'.title') }}
                </div> --}}
            </div>
        </div>
    
        </div>
@endcomponent
