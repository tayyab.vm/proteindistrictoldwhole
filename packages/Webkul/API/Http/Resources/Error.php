<?php

namespace Webkul\API\Http\Resources\Core;

use http\Message;
use Illuminate\Http\Resources\Json\JsonResource;

class Error extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public static function JsonError($error=null,$code=null,$status=null,$messageAr=null,$messageEn=null)
    {
        return response()->json([
                [
                    'code' => $code,
                    'error' => $error,
                    'status' => $status,
                    'messageAr' => $messageAr,
                    'message' => $messageEn,

                ]
            ]);

    }
}