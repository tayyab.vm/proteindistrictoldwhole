<?php

namespace Webkul\API\Http\Resources\Core;

use Illuminate\Http\Resources\Json\JsonResource;
use Webkul\Core\Models\ChannelLocale;

class Locale extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $active = 'false';
        $locales = ChannelLocale::where('channel_id',core()->getCurrentChannel()->id)->where('locale_id',$this->id)->get()->first();
        if($locales)
        $active='true';


        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'active' => $active,
            'privacy' => strip_tags($this->privacy),
            'terms' => strip_tags($this->terms),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}