<?php

namespace Webkul\API\Http\Resources\Core;

use Illuminate\Http\Resources\Json\JsonResource;

class Brand extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $locale=($request->input('locale')=='ar')?'ar':'en';
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => ($locale=='ar')?$this->name_ar:$this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}