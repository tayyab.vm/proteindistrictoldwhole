<?php

namespace Webkul\API\Http\Resources\Core;

use Illuminate\Http\Resources\Json\JsonResource;

class Slider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image_url' => $this->image_url,
            'content' => $this->content,
            'product_id' => $this->product_id,
            'header_for_deals' => $this->deal,
            'product_url'=>env('APP_URL')."/api/products/".$this->product_id,

        ];
    }
}