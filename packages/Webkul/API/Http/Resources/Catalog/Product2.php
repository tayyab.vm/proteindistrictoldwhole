<?php

namespace Webkul\API\Http\Resources\Catalog;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Webkul\Attribute\Models\AttributeOption;
use Webkul\Attribute\Models\DType;

class Product2 extends JsonResource
{
    /**
     * Create a new resource instance.
     *
     * @return void
     */
    public function __construct($resource)
    {
        $this->productPriceHelper = app('Webkul\Product\Helpers\Price');

        $this->productImageHelper = app('Webkul\Product\Helpers\ProductImage');
        $this->conf = app('Webkul\Product\Helpers\ConfigurableOption');
        $this->productReviewHelper = app('Webkul\Product\Helpers\Review');
        $this->productViewHelper = app('Webkul\Product\Helpers\View');


        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $iswish = 'false';

        $priceHelper = $this->productPriceHelper;
        $conf = $this->conf;
        $product = $this->product ? $this->product : $this;
        if (request()->has('token')) {
            $user = Auth::user();
            if ($user)
                $Wishlist = \Webkul\Customer\Models\Wishlist::where('customer_id', $user->id)->where('product_id', $product->id)->get();
            if (count($Wishlist) > 0)
                $iswish = 'true';
        }


        $promotion = '';
        if ($priceHelper->haveXYSpecialPrice($product, $product->buyx))
            $promotion = 'Buy ' . $priceHelper->getxy($product->buyx) . ' get ' . $priceHelper->getxy($product->gety) . ' Free';
        else if ($priceHelper->haveSpecialPrice($product)) {
            if (!is_null($product->buyx) && $product->buyx > 0 && ($product->gety == 0 || is_null($product->gety)))
                $promotion = 'Buy ' . $product->buyx . ' get ' . number_format(($product->special_price / $product->price) * 100) . '% OFF';
            else
                $promotion = 'sale';
        } else
            $promotion = '';

     //   if (($this->conf->getConfigurationConfig2($product)['attributes'] && $product->type == 'configurable') || ($product->type == 'simple' && $this->qty > 0))
            return [
                'dtype' => DType::getName($product->dtype),
                'id' => $product->id,
                'qty' => $this->qty,
                'type' => $product->type,
                'name' => $this->name,
                'promotion' => $promotion,
                'price' => $product->type == 'configurable' ? $this->productPriceHelper->getVariantMinPrice($product) : $this->price,
                'formated_price' => $product->type == 'configurable' ? core()->currency($this->productPriceHelper->getVariantMinPrice($product)) : core()->currency($this->price),
                'short_description' => strip_tags($this->short_description),
                'description' => strip_tags($this->description),
                'sku' => $this->sku,
                'base_image' => $this->productImageHelper->getProductBaseImage($product),
                'images' => ProductImage::collection($product->images),
                'in_stock' => $product->type == 'configurable' ? true : $product->haveSufficientQuantity(1),
                //'variants' => Self::collection($this->variants),
                'special_price' => $this->when(
                    $this->productPriceHelper->haveSpecialPrice($product),
                    $this->productPriceHelper->getSpecialPrice($product)
                ),
                'reviews' => [
                    'total' => number_format(rand(50, 400), 0),
                    'average_rating' => number_format(rand(4, 5), 0),
                    'percentage' => number_format(rand(90, 100), 0),
                ],


                'is_saved' => false,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                $this->mergeWhen($product->type == 'configurable', [
                    'super_attributes' => $conf->getConfigurationConfig2($product)['attributes'],
                ]),

                'iswish' => $iswish,
                //'other' => $product::find($product->id),
                'other' =>
                    [

                        'description' => strip_tags($this->description),
                        "SupplementFacts2" => $this->SupplementFacts2,
                        "use_direction" => $this->use_direction,
                        "Ingredients" => $this->Ingredients,
                        "color" => $this->color,

                        "color_option" => !is_null($this->color) ? AttributeOption::find($this->color)->admin_name : null,
                        "size" => $this->size,
                        "size_option" => !is_null($this->size) ? AttributeOption::find($this->size)->admin_name : null,
                        "Flavor" => $this->Flavor,
                        "Flavor_option" => !is_null($this->Flavor) ? AttributeOption::find($this->Flavor)->admin_name : null,
                        "weight" => $this->weight,
                    ]


            ];
      //  else
        //    return null;
    }
}