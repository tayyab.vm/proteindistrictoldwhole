<?php

namespace Webkul\API\Http\Controllers\Shop;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Webkul\API\Http\Resources\Core\Error;

/**
 * Forgot Password controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try {

            $validator = Validator::make(request()->all(), [
                'email' => 'required|email'
            ]);
            if ($validator->fails()) {
                return Error::JsonError(422, 422, 'Failed', 'البيانات غير صالحة', $validator->messages()->first());

            }

            $response = $this->broker()->sendResetLink(request(['email']));

            if ($response == Password::RESET_LINK_SENT) {
                return response()->json([
                    'error' => 0,
                    'message' => trans($response)
                ]);
            }

            return response()->json([

                'error' => 422,
                'message' => trans($response),
                'code' => 422,
                'messageAr' => trans($response),
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('customers');
    }
}