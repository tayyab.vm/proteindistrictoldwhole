<?php

namespace Webkul\API\Http\Controllers\Shop;

use Cart;
use Webkul\API\Http\Resources\Checkout\Cart as CartResource;
use Webkul\API\Http\Resources\Checkout\CartShippingRate as CartShippingRateResource;
use Webkul\API\Http\Resources\Core\Error;
use Webkul\API\Http\Resources\Sales\Order as OrderResource;
use Webkul\Checkout\Repositories\CartItemRepository;
use Webkul\Checkout\Repositories\CartRepository;
use Webkul\Discount\Helpers\CouponAbleRule as Coupon;
use Webkul\Discount\Helpers\NonCouponAbleRule as NonCoupon;
use Webkul\Discount\Helpers\ValidatesDiscount;
use Webkul\Payment\Facades\Payment;
use Webkul\Sales\Repositories\OrderRepository;
use Webkul\Shipping\Facades\Shipping;
use App\Log;


/**
 * Checkout controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CheckoutController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;

    /**
     * CartRepository object
     *
     * @var Object
     */
    protected $cartRepository;

    /**
     * CartItemRepository object
     *
     * @var Object
     */
    protected $cartItemRepository;
    protected $coupon;
    protected $validatesDiscount;
    /**
     * NoncouponAbleRule instance object
     *
     */
    protected $nonCoupon;

    /**
     * Controller instance
     *
     * @param Webkul\Checkout\Repositories\CartRepository $cartRepository
     * @param Webkul\Checkout\Repositories\CartItemRepository $cartItemRepository
     * @param Webkul\Sales\Repositories\OrderRepository $orderRepository
     */
    public function __construct(
        CartRepository $cartRepository,
        CartItemRepository $cartItemRepository,
        OrderRepository $orderRepository,
        Coupon $coupon,
        NonCoupon $nonCoupon,
         ValidatesDiscount $validatesDiscount

    )
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';
        $this->validatesDiscount = $validatesDiscount;
        auth()->setDefaultDriver($this->guard);

        $this->coupon = $coupon;

        $this->nonCoupon = $nonCoupon;
        // $this->middleware('auth:' . $this->guard);

        $this->_config = request('_config');

        $this->cartRepository = $cartRepository;

        $this->cartItemRepository = $cartItemRepository;

        $this->orderRepository = $orderRepository;
    }

    /**
     * Saves customer address.
     *
     * @param  \Webkul\Checkout\Http\Requests\CustomerAddressForm $request
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        $logs = new Log;
        $logs->message = 'checkoout';
        $logs->request = json_encode(request()->all());
        $logs->save();
        
        try {

            $data = request()->all();

            $data['billing'] = array(
                "address_id" => request()->billing_address_id,
                "use_for_billing" => 'true');
            $data['shipping'] = array(
                "address_id" => request()->shipping_address_id,
                "use_for_shipping" => 'true');


            if (isset($data['billing']['id']) && str_contains($data['billing']['id'], 'address_')) {
                unset($data['billing']['id']);
                unset($data['billing']['address_id']);
            }

            if (isset($data['shipping']['id']) && str_contains($data['shipping']['id'], 'address_')) {
                unset($data['shipping']['id']);
                unset($data['shipping']['address_id']);
            }
            
            if (Cart::hasError())
                return Error::JsonError(400, '400', 'faild', 'خطأ', 'Cart is empty');

            if (!Cart::saveCustomerAddress($data) || !Shipping::collectRates())
                return Error::JsonError(400, '400', 'faild', 'خطأ', 'Error in address');


            $shippingMethod = (request()->shipping_method==1)?'flatrate_flatrate':'free_free';

            if (!$shippingMethod || !Cart::saveShippingMethod($shippingMethod))
                return Error::JsonError(400, '400', 'faild', 'خطأ', 'Error in shipping method');
            Cart::collectTotals();
          //  $payment = (request()->payment==1)?'cashondelivery':'moneytransfer';
            $payment = (request()->payment==1)?'cashondelivery':'moneytransfer';
            $payment = ["method" => $payment];
            if (Cart::hasError() || !$payment || !Cart::savePaymentMethod($payment))
                return Error::JsonError(400, '400', 'faild', 'خطأ', 'Error in payment method');

            $inventory = request()->inventory;
            //$inventory = ["method" => $inventory];
            if (Cart::hasError()  || !Cart::saveInventoryMethod($inventory))
                return Error::JsonError(400, '400', 'faild', 'خطأ', 'Error in inventory choose');

            try {
                if (Cart::hasError())
                    abort(400);
                $this->nonCoupon->apply();

                Cart::collectTotals();

                $this->validateOrder();

                $cart = Cart::getCart();

                if ($redirectUrl = Payment::getRedirectUrl($cart)) {
                    return response()->json([
                        'error' => 0,
                        'success' => true,
                        'redirect_url' => $redirectUrl
                    ]);
                }

                $order = $this->orderRepository->create(Cart::prepareDataForOrder());
                $order->transaction_id=request()->transaction_id;
                $order->save();
                Cart::deActivateCart();
                $logs->response = $order;
                $logs->update();
                return response()->json([
                    'error' => 0,
                    'success' => true,
                    'order' => new OrderResource($order),
                ]);
            } catch (\Exception $x) {
                return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
            }


        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getLine(), 'faild', 'خطأ', 'Error');
        }
    }

    public function saveAddress()
    {
        try {
            $data = request()->all();
            if (true) {
                $data['billing'] = array(
                    "address_id" => request()->shipping_address_id,
                    "use_for_shipping" => 'true');
                $data['shipping'] = array(
                    "address_id" => request()->shipping_address_id,
                    "use_for_shipping" => 'true');
            }


            if (isset($data['billing']['id']) && str_contains($data['billing']['id'], 'address_')) {
                unset($data['billing']['id']);
                unset($data['billing']['address_id']);
            }

            if (isset($data['shipping']['id']) && str_contains($data['shipping']['id'], 'address_')) {
                unset($data['shipping']['id']);
                unset($data['shipping']['address_id']);
            }

            if (Cart::hasError() || !Cart::saveCustomerAddress($data) || !Shipping::collectRates())
                return Error::JsonError(422, '422', 'faild', 'خطأ', 'Error');

            $rates = [];

            foreach (Shipping::getGroupedAllShippingRates() as $code => $shippingMethod) {
                $rates[] = [
                    'carrier_title' => $shippingMethod['carrier_title'],
                    'rates' => CartShippingRateResource::collection(collect($shippingMethod['rates']))
                ];
            }


            Cart::collectTotals();

            return response()->json([
                'error' => 0,
                'data' => [
                    'rates' => $rates,
                    'cart' => new CartResource(Cart::getCart())
                ]
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getLine(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Saves shipping method.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveShipping()
    {
        try {
            $shippingMethod = request()->shipping_method;

            if (Cart::hasError() || !$shippingMethod || !Cart::saveShippingMethod($shippingMethod))
                abort(400);

            Cart::collectTotals();

            return response()->json([
                'error' => 0,
                'data' => [
                    'methods' => Payment::getPaymentMethods(),
                    'cart' => new CartResource(Cart::getCart())
                ]
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getLine(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Saves payment method.
     *
     * @return \Illuminate\Http\Response
     */
    public function savePayment()
    {
        try {
            $payment = request()->payment;
            $payment = ["method" => $payment];
            Cart::savePaymentMethod($payment);
            if (Cart::hasError() || !$payment || !Cart::savePaymentMethod($payment))
                abort(400);

            return response()->json([
                'error' => 0,
                'data' => [
                    'cart' => new CartResource(Cart::getCart())
                ]
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getLine(), 'faild', 'خطأ', 'Error');
        }
    }

    public function saveInventory()
    {
        try {
            $inventory = request()->inventory;
            //$inventory = ["method" => $inventory];
            Cart::saveInventoryMethod($inventory);
            if (Cart::hasError() || !$inventory || !Cart::saveInventoryMethod($inventory))
                abort(400);

            return response()->json([
                'error' => 0,
                'data' => [
                    'cart' => new CartResource(Cart::getCart())
                ]
            ]);
        } catch (\Exception $x) {
            return Error::JsonError(422, 422, 'faild', 'خطأ', 'Please check inventroy id');
        }
    }

    public function applyCoupon()
    {
        try {
            $this->validate(request(), [
                'code' => 'string|required'
            ]);

            $code = request()->input('code');

            $result = $this->coupon->apply($code);

            if($result == 'promotion'){
                return Error::JsonError(422, 422, 'faild', 'خطأ', 'Not applicable on promotion items');
            }

            if ($result) {
                Cart::collectTotals();
                return response()->json([
                    'error' => 0,
                    'data' => [
                        'cart' => new CartResource(Cart::getCart())
                    ]
                ]);


            } else {
                return Error::JsonError(422, 422, 'faild', 'خطأ', 'Already applied');
            }
        } catch (\Exception $x) {
            return Error::JsonError(422, 422, 'faild', 'خطأ', 'Error in Data');
        }

    }

    public function removeCoupon()
    {
        $result = $this->coupon->remove();

        if ($result) {
            Cart::collectTotals();

            return response()->json([
                'error' => 0,
                'data' => [
                    'cart' => new CartResource(Cart::getCart())
                ]
            ]);
        } else {
            return Error::JsonError(422, 422, 'faild', 'خطأ', 'Error in Data');
        }
    }


    /**
     * Saves order.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveOrder()
    {
        try {
            if (Cart::hasError())
                abort(400);

            Cart::collectTotals();

            $this->validateOrder();

            $cart = Cart::getCart();

            if ($redirectUrl = Payment::getRedirectUrl($cart)) {
                return response()->json([
                    'error' => 0,
                    'success' => true,
                    'redirect_url' => $redirectUrl
                ]);
            }

            $order = $this->orderRepository->create(Cart::prepareDataForOrder());

            Cart::deActivateCart();

            return response()->json([
                'error' => 0,
                'success' => true,
                'order' => new OrderResource($order),
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Validate order before creation
     *
     * @return mixed
     */
    public function validateOrder()
    {
        try {
            $cart = Cart::getCart();
            $this->validatesDiscount->validate($cart);

            if (!$cart->shipping_address) {
                throw new \Exception(trans('Please check shipping address.'));
            }

            if (!$cart->billing_address) {
                throw new \Exception(trans('Please check billing address.'));
            }

            if (!$cart->selected_shipping_rate) {
                throw new \Exception(trans('Please specify shipping method.'));
            }

            if (!$cart->payment) {
                throw new \Exception(trans('Please specify payment method.'));
            }

        } catch (\Exception $x) {
            return Error::JsonError(500, $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

}