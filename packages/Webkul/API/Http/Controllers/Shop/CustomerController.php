<?php

namespace Webkul\API\Http\Controllers\Shop;

use Illuminate\Support\Facades\Event;
use Webkul\API\Http\Resources\Core\Error;

use Illuminate\Support\Facades\Validator;
use Webkul\Customer\Repositories\CustomerRepository;

/**
 * Customer controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CustomerController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Repository object
     *
     * @var array
     */
    protected $customerRepository;

    /**
     * @param CustomerRepository object $customer
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        try {
            $this->_config = request('_config');

            $this->customerRepository = $customerRepository;
        }
        catch (\Exception $x) {
            return Error::JsonError(500, 500, 'faild', 'خطأ', 'General Server Error');
        }
    }

    /**
     * Method to store user's sign up form data to DB.
     *
     * @return Mixed
     */
    public function create()
    {
        try {
            $validator = Validator::make(request()->all(), [
                'first_name' => 'required',
                'email' => 'email|required|unique:customers,email',
                'password' => 'min:6|required'
            ]);
            if ($validator->fails()) {
                return Error::JsonError( 422,422,'Failed', 'البيانات غير صالحة', $validator->messages()->first());

            }


            $data = request()->input();

            $data = array_merge($data, [
                'password' => bcrypt($data['password']),
                'channel_id' => core()->getCurrentChannel()->id,
                'is_verified' => 1,
                'customer_group_id' => 1
            ]);

            Event::fire('customer.registration.before');

            $customer = $this->customerRepository->create($data);

            Event::fire('customer.registration.after', $customer);

            return response()->json([
                'error'=>0,
                'message' => 'Your account has been created successfully.',
                'messageAr' => 'تم إنشاء الحساب بنجاح.',
                'status' => 'Success',
            ]);
        } catch (\Exception $x) {
            return Error::JsonError( 422,$x->getCode(),'Failed', 'البيانات غير صالحة', $x->getMessage());
        }
    }
}