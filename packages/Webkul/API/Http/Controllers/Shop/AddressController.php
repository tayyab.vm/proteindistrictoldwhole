<?php

namespace Webkul\API\Http\Controllers\Shop;

use Webkul\API\Http\Resources\Core\Error;
use Webkul\API\Http\Resources\Customer\CustomerAddress as CustomerAddressResource;
use Illuminate\Support\Facades\Validator;
use Webkul\Customer\Models\CustomerAddress;
use Webkul\Customer\Repositories\CustomerAddressRepository;

/**
 * Address controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class AddressController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerAddressRepository object
     *
     * @var Object
     */
    protected $customerAddressRepository;

    /**
     * Controller instance
     *
     * @param Webkul\Customer\Repositories\CustomerAddressRepository $customerAddressRepository
     */
    public function __construct(
        CustomerAddressRepository $customerAddressRepository
    )
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';

        auth()->setDefaultDriver($this->guard);

        $this->middleware('auth:' . $this->guard);

        $this->_config = request('_config');

        $this->customerAddressRepository = $customerAddressRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try {
            $customer = auth($this->guard)->user();

            request()->merge([
                'address1' => request()->input('address1'),
                'customer_id' => $customer->id
            ]);


            /*$this->validate(request(), [
                'address1' => 'required',
                'country' => 'required',
                'state' => 'required',
                'city' => 'required',
                'postcode' => 'required',
                'phone' => 'required'
            ]);*/
            $validator = Validator::make(request()->all(), [
                'address1' => 'required',
                'country' => 'required',
                'city' => 'required',
                'postcode' => 'required',

            ]);
            if ($validator->fails()) {
                return Error::JsonError(422, 422, 'Failed', 'البيانات غير صالحة', $validator->messages()->first());

            }
            if(request()->input('is_billing'))
            {
                CustomerAddress::where('customer_id',$customer->id)->update(['is_billing' => 0]);
            }
            $customerAddress = $this->customerAddressRepository->create(request()->all());

            return response()->json([
                'error' => 0,
                'message' => 'Your address has been created successfully.',

                'data' => $customerAddress
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getMessage(), $x->getLine(), 'faild', 'خطأ في العنوان', 'Address Error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        try {
            $customer = auth($this->guard)->user();



            $validator = Validator::make(request()->all(), [
                'address1' => 'required',
                'country' => 'required',
                'city' => 'required',
                'postcode' => 'required',
            ]);

            if ($validator->fails()) {
                return Error::JsonError(422, 422, 'Failed', 'البيانات غير صالحة', $validator->messages()->first());

            }
            if(request()->input('is_billing'))
            {
                CustomerAddress::where('customer_id',$customer->id)->update(['is_billing' => 0]);
            }

            $this->customerAddressRepository->update(request()->all(),$id);



            return response()->json([
                'error' => 0,
                'message' => 'Your address has been updated successfully.',
                'data' => new CustomerAddressResource($this->customerAddressRepository->find($id))
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ في تحديث العنوان', 'Address Update Error');
        }
    }
}