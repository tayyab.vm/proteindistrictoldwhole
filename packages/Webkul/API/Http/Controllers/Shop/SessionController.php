<?php

namespace Webkul\API\Http\Controllers\Shop;

use Illuminate\Support\Facades\Validator;
use Webkul\API\Http\Resources\Core\Error as Error;
use Webkul\API\Http\Resources\Customer\Customer as CustomerResource;
use Webkul\Customer\Repositories\CustomerRepository;
use Cart;
use Webkul\API\Http\Resources\Checkout\Cart as CartResource;
use Webkul\API\Http\Resources\Checkout\CartShippingRate as CartShippingRateResource;
use Webkul\Checkout\Repositories\CartItemRepository;
use Webkul\Checkout\Repositories\CartRepository;
use Webkul\Discount\Helpers\NonCouponAbleRule as NonCoupon;
use Webkul\Discount\Helpers\ValidatesDiscount as ValidatesDiscount;
use Webkul\Payment\Facades\Payment;
use Webkul\Shipping\Facades\Shipping;

/**
 * Session controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SessionController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;
        protected $nonCoupon;
    protected $validatesDiscount;

    /**
     * Controller instance
     *
     * @param Webkul\Customer\Repositories\CustomerRepository $customerRepository
     */
    public function __construct(CustomerRepository $customerRepository, NonCoupon $nonCoupon,
        ValidatesDiscount $validatesDiscount)
    {
        try {
            $this->guard = request()->has('token') ? 'api' : 'customer';

            auth()->setDefaultDriver($this->guard);

            $this->middleware('auth:' . $this->guard, ['only' => ['get', 'update', 'destroy']]);

            $this->_config = request('_config');


            $this->customerRepository = $customerRepository;
        } catch (\Exception $x) {
            return Error::JsonError(500, 500, 'faild', 'خطأ', 'General Server Error');
        }
    }

    /**
     * Method to store user's sign up form data to DB.
     *
     * @return Mixed
     */
    public
    function create()
    {
        // try {

        $validator = Validator::make(request()->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return Error::JsonError(422, 422, 'Failed', 'البيانات غير صالحة', $validator->messages()->first());

        }

        $jwtToken = null;

        if (!$jwtToken = auth()->guard($this->guard)->attempt(request()->only('email', 'password'))) {
            return response()->json([
                'message' => 'Invalid Email or Password',
                'code' => 401,
                'error' => 401,
                'messageAr' => 'خطأ في اسم المستخدم أو  كلمة المرور',

            ]);
        }
        if ((request()->input('cart_id'))) {


            Cart::mergeCart2(request()->input('cart_id'));
            if ($cart = Cart::getCart()) {

                $data = request()->all();
                $data['billing'] = array(
                    "address_id" => request()->shipping_address_id,
                    "use_for_shipping" => 'true');
                $data['shipping'] = array(
                    "address_id" => request()->shipping_address_id,
                    "use_for_shipping" => 'true');


                if (isset($data['billing']['id']) && str_contains($data['billing']['id'], 'address_')) {
                    unset($data['billing']['id']);
                    unset($data['billing']['address_id']);
                }

                if (isset($data['shipping']['id']) && str_contains($data['shipping']['id'], 'address_')) {
                    unset($data['shipping']['id']);
                    unset($data['shipping']['address_id']);
                }


                Cart::saveCustomerAddress($data);
                Shipping::collectRates();
                $rates = [];
                $id = 0;

                foreach (Shipping::getGroupedAllShippingRates() as $code => $shippingMethod) {
                    $id++;
                    $rates[] = [
                        'id' => $id,
                        'carrier_title' => $shippingMethod['carrier_title'],
                        'rates' => CartShippingRateResource::collection(collect($shippingMethod['rates']))
                    ];
                    Cart::collectTotals();
                }

                Cart::collectTotals();

            } else {
                $rates = [];
            }
        }
        //Event::fire('customer.after.login', request()->input('email'));

        $customer = auth($this->guard)->user();

        return response()->json([
            'token' => $jwtToken,
            'error' => 0,
            'message' => 'Logged in successfully.',
            'data' => new CustomerResource($customer)
        ]);
        // } catch (\Exception $x) {
        //    return Error::JsonError(422, 422, 'faild', 'خطأ تسجيل الدخول', 'login Error Unprocessable Entity (validation failed)');
        // }
    }

    /**
     * Get details for current logged in customer
     *
     * @return \Illuminate\Http\Response
     */
    public
    function get()
    {
        try {
            try {

                $customer = auth($this->guard)->user();
            } catch (\InvalidArgumentException $x) {
                return Error::JsonError(500, $x->code(), 'faild', 'معلومات الحساب خاظئة', 'Bad login info');
            }

            return response()->json([
                'data' => new CustomerResource($customer),
                'error' => 0
            ]);
        } catch (\InvalidArgumentException $x) {
            return Error::JsonError(500, $x->getCode(), 'faild', 'معلومات الحساب خاظئة', 'Bad login info');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function update()
    {
        try {
            $customer = auth($this->guard)->user();


            $validator = Validator::make(request()->all(), [
                'first_name' => 'required',
                'email' => 'required|email|unique:customers,email,' . $customer->id,
                'attachment' => 'file|max:4100',
            ]);
            if ($validator->fails()) {
                return Error::JsonError(422, 422, 'Failed', 'البيانات غير صالحة', $validator->messages()->first());

            }

            $data = request()->all();


            if (!isset($data['password']) || !$data['password']) {
                unset($data['password']);
            } else {
                $data['password'] = bcrypt($data['password']);
            }
            if (request()->file('attachment')) {
                $picName = request()->file('attachment')->getClientOriginalName();
                $picName = uniqid() . '_' . $picName;
                $path = 'uploads' . DIRECTORY_SEPARATOR . 'profile';
                $destinationPath = rtrim(app()->basePath('public_html/' . $path), '/');//public_path($path); // upload path
                //dd($request);
                //File::makeDirectory($destinationPath, 0777, true, true);
                request()->file('attachment')->move($destinationPath, $picName);
                $customer->image = $path . DIRECTORY_SEPARATOR . $picName;
                $customer->save();
            }


            $this->customerRepository->update($data, $customer->id);


            return response()->json([
                'message' => 'Your account has been created successfully.',
                'error' => 0,
                'data' => new CustomerResource($this->customerRepository->find($customer->id))
            ]);
        } catch (\Exception $x) {
            return Error::JsonError(422, $x->getCode(), 'faild', 'خطأ في البيانات ', 'Data Error Unprocessable Entity (validation failed)');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function destroy()
    {
        try {
            auth()->guard($this->guard)->logout();

            return response()->json([
                'message' => 'Logged out successfully.',
                'error' => 0,
            ]);
        } catch (\Exception $x) {
            return Error::JsonError(422, $x->getCode(), 'faild', 'خطأ تسجيل الخروج', 'Error when Sign Out');
        }
    }
}