<?php

namespace Webkul\API\Http\Controllers\Shop;

use Cart;
use Webkul\API\Http\Resources\Core\Error;
use Webkul\API\Http\Resources\Customer\Wishlist as WishlistResource;
use Webkul\Customer\Repositories\WishlistRepository;
use Webkul\Product\Repositories\ProductRepository;

/**
 * Wishlist controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class WishlistController extends Controller
{
    /**
     * WishlistRepository object
     *
     * @var object
     */
    protected $wishlistRepository;

    /**
     * ProductRepository object
     *
     * @var object
     */
    protected $productRepository;

    /**
     * @param Webkul\Customer\Repositories\WishlistRepository $wishlistRepository
     * @param Webkul\Product\Repositories\ProductRepository $productRepository
     */
    public function __construct(
        WishlistRepository $wishlistRepository,
        ProductRepository $productRepository
    )
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';

        auth()->setDefaultDriver($this->guard);

        $this->middleware('auth:' . $this->guard);

        $this->wishlistRepository = $wishlistRepository;

        $this->productRepository = $productRepository;
    }

    /**
     * Function to add item to the wishlist.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        try {
            $product = $this->productRepository->findOrFail($id);

            $customer = auth()->guard($this->guard)->user();

            $wishlistItem = $this->wishlistRepository->findOneWhere([
                'channel_id' => core()->getCurrentChannel()->id,
                'product_id' => $id,
                'customer_id' => $customer->id
            ]);

            if (!$wishlistItem) {
                $wishlistItem = $this->wishlistRepository->create([
                    'channel_id' => core()->getCurrentChannel()->id,
                    'product_id' => $id,
                    'customer_id' => $customer->id
                ]);

                return response()->json([
                    'error' => 0,
                    'data' => new WishlistResource($wishlistItem),
                    'message' => trans('customer::app.wishlist.success')
                ]);
            } else {

                return response()->json([
                    'error' => 0,
                    'code' => 0,
                    'data' => new WishlistResource($wishlistItem),
                    'message' => 'Item is exist.'
                ]);
            }
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getLine(), 'faild', 'خطأ', $x->getMessage());
        }
    }

    /**
     * Move product from wishlist to cart.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function moveToCart($id)
    {
        try {


            $wishlistItem = $this->wishlistRepository->findOrFail($id);


            if ($wishlistItem->customer_id != auth()->guard($this->guard)->user()->id)
                return response()->json([
                    'error' => 400,
                    'code' => 400,
                    'message' => 'error',
                    'messageAr' => 'خطأ'
                ]);

            $result = Cart::moveToCart($wishlistItem);

            if ($result == 1) {
                if ($wishlistItem->delete()) {
                    Cart::collectTotals();

                    return response()->json([
                        'error' => 0,
                        'data' => new WishlistResource($wishlistItem),

                        'message' => trans('shop::app.wishlist.moved')
                    ]);
                } else {
                    return response()->json([
                        'message' => '',
                        'data' => 1,
                        'messageAr' => 'خطأ',
                        'error' => 400,
                        'code' => 400,
                    ]);
                }
            } else if ($result == 0) {
                return response()->json([
                    'error' => 400,

                    'code' => 400,
                    'data' => 0,
                    'messageAr' => 'خطأ',

                ]);
            } else if ($result == -1) {
                return response()->json([
                    'data' => -1,
                    'error' => 400,
                    'code' => 400,
                    'messageAr' => 'خطأ',
                ]);
            }
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', $x->getMessage());
        }
    }

    public function remove($id)
    {
        try {
            $product = $this->productRepository->findOrFail($id);

        $customer = auth()->guard($this->guard)->user();

            $wishlistItem = $this->wishlistRepository->findOneWhere([
                'channel_id' => core()->getCurrentChannel()->id,
                'product_id' => $id,
                'customer_id' => $customer->id
            ]);

            if (!$wishlistItem) {

                return response()->json([
                    'error' => 0,
                    'message' => trans('No item Found')
                ]);
            } else {
                $this->wishlistRepository->delete($wishlistItem->id);

                return response()->json([
                    'error' => 0,
                    'message' => 'Item removed from wishlist successfully.'
                ]);
            }
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getLine(), 'faild', 'خطأ', $x->getMessage());
        }
    }






}