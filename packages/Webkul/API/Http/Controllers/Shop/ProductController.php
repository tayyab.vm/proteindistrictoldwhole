<?php

namespace Webkul\API\Http\Controllers\Shop;

use Webkul\API\Http\Resources\Catalog\Product as ProductResource;
use Webkul\API\Http\Resources\Core\Error;
use Webkul\Attribute\Models\DType;
use Webkul\Product\Repositories\ProductRepository;
use App\Log;

/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductController extends Controller
{
    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $productRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Product\Repositories\ProductRepository $productRepository
     * @return void
     */
    public function __construct(ProductRepository $productRepository)
    {
        if (request()->has('token')) {
            $this->guard = request()->has('token') ? 'api' : 'customer';

            auth()->setDefaultDriver($this->guard);

            $this->middleware('auth:' . $this->guard);
        }

        $this->productRepository = $productRepository;
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $params = request()->input();
        $array = [
            'error' => 0,
            'page' => (request()->input('page')) ? request()->input('page') : 1,
            'total' => $this->productRepository->getAll2(request()->input('category_id'), request()->input('brand_id'), request()->input('goal_id'), request()->input('dtype_id'))->count(),
            'limit' => (request()->input('limit')) ? request()->input('limit') : 9,
            'data' => $this->productRepository->getAll2(request()->input('category_id'), request()->input('brand_id'), request()->input('goal_id'), request()->input('dtype_id'))->forPage((isset($params['page']) ? $params['page'] : 1), (isset($params['limit']) ? $params['limit'] : 9))
        ];
        $logs = json_encode($array);
        \Log::debug('products_res',[$logs]);
        
        return response()->json(
            // [
            //     'error' => 0,
            //     'page' => (request()->input('page')) ? request()->input('page') : 1,
            //     'total' => $this->productRepository->getAll2(request()->input('category_id'), request()->input('brand_id'), request()->input('goal_id'), request()->input('dtype_id'))->count(),
            //     'limit' => (request()->input('limit')) ? request()->input('limit') : 9,
            //     'data' => $this->productRepository->getAll2(request()->input('category_id'), request()->input('brand_id'), request()->input('goal_id'), request()->input('dtype_id'))->forPage((isset($params['page']) ? $params['page'] : 1), (isset($params['limit']) ? $params['limit'] : 9))
            // ]
            $array
        );


    }

    public function main()
    {
        $params = request()->input();
        $logs = new Log;
        $logs->message = 'home api';
        $logs->request = json_encode(request()->all());
        $logs->save();
        $count = 0;
        $x = 0;
        $a = [];
        foreach (DType::getlist() as $d) {
            $count = $count + $this->productRepository->getTop10(null, null, null, $d->id)->count();
            $a[$x] = ["type" => (isset($params['lang']) && $params['lang'] == "ar" && !is_null($d->name_ar)) ? $d->name_ar : $d->name
                , "dtype_id" => $d->id, "child" => $this->productRepository->getTop10(null, null, null, $d->id)->forPage((isset($params['page']) ? $params['page'] : 1), (isset($params['limit']) ? 9 : 9))];
            $x++;

        }
        return response()->json(
            [
                'error' => 0,
                'page' => (request()->input('page')) ? request()->input('page') : 1,
                'total' => $count,
                'limit' => (request()->input('limit')) ? request()->input('limit') : 9,
                'data' => $a,

            ]);


    }


    /**
     * Returns a individual resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        try {
            return
                response()->json(
                    [
                        'error' => 0,
                        'data' => new ProductResource(
                            $this->productRepository->findOrFail($id)
                        )]);
        } catch (\Exception $x) {
            return Error::JsonError(400, 400, 'faild', 'المنتج غير موجود', 'Not Available Product');
        }
    }

    /**
     * Returns product's additional information.
     *
     * @return \Illuminate\Http\Response
     */
    public function additionalInformation($id)
    {
        try {
            return response()->json([
                'error' => '0',
                'data' => app('Webkul\Product\Helpers\View')->getAdditionalData($this->productRepository->findOrFail($id))
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Returns product's additional information.
     *
     * @return \Illuminate\Http\Response
     */
    public function configurableConfig($id)
    {
        try {
            return response()->json([
                'error' => '0',
                'data' => app('Webkul\Product\Helpers\ConfigurableOption')->getConfigurationConfig($this->productRepository->findOrFail($id))
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }
}
