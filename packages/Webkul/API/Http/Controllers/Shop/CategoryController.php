<?php

namespace Webkul\API\Http\Controllers\Shop;

use Webkul\API\Http\Resources\Core\Error;
use Webkul\Category\Repositories\CategoryRepository;

/**
 * Category controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CategoryController extends Controller
{
    /**
     * CategoryRepository object
     *
     * @var array
     */
    protected $categoryRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Category\Repositories\CategoryRepository $categoryRepository
     * @return void
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return response()->json(
                [
                    'error' => 0,
                    'data' => $this->categoryRepository->getVisibleCategoryTree(request()->input('parent_id'))
                ]);

        } catch (\Exception $x) {
            return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }
}
