<?php

namespace Webkul\API\Http\Controllers\Shop;

use Cart;
use Illuminate\Support\Facades\Event;
use Webkul\API\Http\Resources\Checkout\Cart as CartResource;
use Webkul\API\Http\Resources\Checkout\CartShippingRate as CartShippingRateResource;
use Webkul\API\Http\Resources\Core\Error;
use Webkul\Checkout\Repositories\CartItemRepository;
use Webkul\Checkout\Repositories\CartRepository;
use Webkul\Discount\Helpers\NonCouponAbleRule as NonCoupon;
use Webkul\Discount\Helpers\ValidatesDiscount as ValidatesDiscount;
use Webkul\Payment\Facades\Payment;
use Webkul\Shipping\Facades\Shipping;
use App\Log;

/**
 * Cart controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CartController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;

    /**
     * CartRepository object
     *
     * @var Object
     */
    protected $cartRepository;
    protected $nonCoupon;
    protected $validatesDiscount;

    /**
     * CartItemRepository object
     *
     * @var Object
     */
    protected $cartItemRepository;

    /**
     * Controller instance
     *
     * @param Webkul\Checkout\Repositories\CartRepository $cartRepository
     * @param Webkul\Checkout\Repositories\CartItemRepository $cartItemRepository
     */
    public function __construct(
        CartRepository $cartRepository,
        NonCoupon $nonCoupon,
        ValidatesDiscount $validatesDiscount,
        CartItemRepository $cartItemRepository
    )
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';

        auth()->setDefaultDriver($this->guard);
        $this->validatesDiscount = $validatesDiscount;
        // $this->middleware('auth:' . $this->guard);

        $this->_config = request('_config');
        $this->nonCoupon = $nonCoupon;
        $this->cartRepository = $cartRepository;

        $this->cartItemRepository = $cartItemRepository;
    }

    /**
     * Get customer cart
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        try {
            if ($cart = Cart::getCart()) {

                $data = request()->all();
                $data['billing'] = array(
                    "address_id" => request()->shipping_address_id,
                    "use_for_shipping" => 'true');
                $data['shipping'] = array(
                    "address_id" => request()->shipping_address_id,
                    "use_for_shipping" => 'true');


                if (isset($data['billing']['id']) && str_contains($data['billing']['id'], 'address_')) {
                    unset($data['billing']['id']);
                    unset($data['billing']['address_id']);
                }

                if (isset($data['shipping']['id']) && str_contains($data['shipping']['id'], 'address_')) {
                    unset($data['shipping']['id']);
                    unset($data['shipping']['address_id']);
                }


                Cart::saveCustomerAddress($data);
                Shipping::collectRates();
                $rates = [];
                $id = 0;

                foreach (Shipping::getGroupedAllShippingRates() as $code => $shippingMethod) {
                    $id++;
                    $rates[] = [
                        'id' => $id,
                        'carrier_title' => $shippingMethod['carrier_title'],
                        'rates' => CartShippingRateResource::collection(collect($shippingMethod['rates']))
                    ];
                    Cart::collectTotals();
                }
                $this->nonCoupon->apply();
                Cart::collectTotals();
                $this->validatesDiscount->validate($cart);
            } else {
                $rates = [];
            }

            $price_to_reduce = $this->checkProductPromotions($cart->id,$cart->items);
            
            $data = [
                'error' => 0,
                'shipping_methods' => $rates,
                'payment_methods' => Payment::getPaymentMethods(),
                'cart' => $cart ? new CartResource($cart) : null
            ];
            
         

            return response()->json(
                $data
            );
        } catch (\Exception $x) {
            // dd($x);
            return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }


    public function checkProductPromotions($cart_id,$cart_items){
        $final_price = 0;
        $newCartItems = \DB::select("SELECT sum(quantity) as quantity,parent_product_id,price from cart_items where cart_id = '$cart_id' group by parent_product_id");
        foreach($newCartItems as $citem){
            $checkpromotion = \App\Promotion::where('parent_id',$citem->parent_product_id)->first();
            if($checkpromotion){
                if($citem->quantity > $checkpromotion->buy_quantity){
                   $sum = $checkpromotion->buy_quantity + $checkpromotion->get_quantity;
                   $waveoffquantity = $citem->quantity / $sum;
                   $waveoffquantity = (int)$waveoffquantity;
                   $waveoffprice = (int)$waveoffquantity * $citem->price;
                   $final_price = $final_price +$waveoffprice;
                   $promotion = true;
                }
            }
        }

        if($final_price>0){
            $newCart = \Webkul\Checkout\Models\Cart::find($cart_id);
            $newCart->sub_total = $newCart->sub_total - $final_price;
            $newCart->base_sub_total = $newCart->base_sub_total - $final_price;
            $newCart->grand_total = $newCart->grand_total - $final_price;
            $newCart->base_grand_total = $newCart->base_grand_total - $final_price;
            $newCart->update();
        }
        // return $final_price;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function store($id)
    {
        //try {
        $test = "0";
        Event::fire('checkout.cart.item.add.before', $id);

        $result = Cart::add($id, request()->except('_token'));

        if (!$result) {
            $message = session()->get('warning') ?? session()->get('error');

            return Error::JsonError(400, 400, 'faild', 'خطأ في البينات', $message);
        }

        Event::fire('checkout.cart.item.add.after', $result);

        Cart::collectTotals();

        $this->nonCoupon->apply();
        Cart::collectTotals();

        $cart = Cart::getCart();
        $this->validatesDiscount->validate($cart);


        return response()->json([
            'message' => 'Product added to cart successfully.',
            'error' => 0,
            'data' => $cart ? new CartResource($cart) : null
        ]);
        //} catch (\Exception $x) {

        // return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ', 'Error');
        //  }

    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function update()
    {
        try {
            foreach (request()->get('qty') as $qty) {
                if ($qty <= 0) {
                    return response()->json([
                            'error' => 401,
                            'message' => trans('shop::app.checkout.cart.quantity.illegal')
                        ]
                    );
                }
            }

            foreach (request()->get('qty') as $itemId => $qty) {
                $item = $this->cartItemRepository->findOneByField('id', $itemId);

                Event::fire('checkout.cart.item.update.before', $itemId);

                Cart::updateItem($item->product_id, ['quantity' => $qty], $itemId);

                Event::fire('checkout.cart.item.update.after', $item);
            }
            Cart::collectTotals();

            $this->nonCoupon->apply();
            Cart::collectTotals();

            $cart = Cart::getCart();
            $this->validatesDiscount->validate($cart);


            return response()->json([
                'message' => 'Cart updated successfully.',
                'error' => 0,
                'data' => $cart ? new CartResource($cart) : null
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function destroy()
    {
        try {
            Event::fire('checkout.cart.delete.before');

            Cart::deActivateCart();

            Event::fire('checkout.cart.delete.after');

            $cart = Cart::getCart();

            return response()->json([
                'error' => 0,
                'message' => 'Cart removed successfully.',
                'data' => $cart ? new CartResource($cart) : null
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroyItem($id)
    {
        try {
            Event::fire('checkout.cart.item.delete.before', $id);

            Cart::removeItem($id);

            Event::fire('checkout.cart.item.delete.after', $id);

            Cart::collectTotals();

            $cart = Cart::getCart();

            return response()->json([
                'error' => 0,
                'message' => 'Cart removed successfully.',
                'data' => $cart ? new CartResource($cart) : null
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Function to move a already added product to wishlist
     * will run only on customer authentication.
     *
     * @param instance cartItem $id
     */
    public
    function moveToWishlist($id)
    {
        try {
            Event::fire('checkout.cart.item.move-to-wishlist.before', $id);

            Cart::moveToWishlist($id);

            Event::fire('checkout.cart.item.move-to-wishlist.after', $id);

            Cart::collectTotals();
            $this->nonCoupon->apply();
            Cart::collectTotals();


            $cart = Cart::getCart();
            $this->validatesDiscount->validate($cart);

            return response()->json([
                'error' => 0,
                'message' => 'Cart item moved to wishlist successfully.',
                'data' => $cart ? new CartResource($cart) : null
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getMessage(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }
}