<?php

namespace Webkul\API\Http\Controllers\Shop;

use Auth;
use Webkul\API\Http\Resources\Core\Error;
use Webkul\Sales\Models\Order;

/**
 * Resource Controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ResourceController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Repository object
     *
     * @var array
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->guard = request()->has('token') ? 'api' : 'customer';

        $this->_config = request('_config');

        if (isset($this->_config['authorization_required']) && $this->_config['authorization_required']) {

            auth()->setDefaultDriver($this->guard);

            $this->middleware('auth:' . $this->guard);

        }

        $this->repository = app($this->_config['repository']);
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function index()
    {
        //try {

        $params = request()->input();


        $query = $this->repository->scopeQuery(function ($query) {

            foreach (request()->except(['page', 'limit', 'pagination', 'sort', 'order', 'token']) as $input => $value) {
                if ($input != 'locale' && (($this->_config['repository'] != 'Webkul\Sales\Repositories\GoalRepository') || ($this->_config['repository'] != 'Webkul\Sales\Repositories\BrandRepository')))
                    $query = $query->where()->whereIn($input, array_map('trim', explode(',', $value)));
            }
            if ($this->_config['repository'] == 'Webkul\Sales\Repositories\OrderRepository') {

                $query = $query->where('customer_id', Auth::user()->id);


            }
            if ($this->_config['repository'] == 'Webkul\Sales\Repositories\InvoiceRepository') {
                $query = $query->wherein('order_id', Order::where('customer_id', Auth::user()->id)->pluck('id')->toArray());
            }
            if ($this->_config['repository'] == 'Webkul\Customer\Repositories\WishlistRepository') {
                $query = $query->where('customer_id', Auth::user()->id);
            }
            if ($this->_config['repository'] == 'Webkul\Customer\Repositories\CustomerAddressRepository') {
                $query = $query->where('customer_id', Auth::user()->id);
            }
            if ($sort = request()->input('sort')) {
                $query = $query->orderBy($sort, request()->input('order') ?? 'desc');
            }
            else if (($this->_config['repository'] == 'Webkul\Core\Repositories\GoalRepository') || ($this->_config['repository'] == 'Webkul\Core\Repositories\BrandRepository')) {
                $query = $query->orderBy('name', 'asc');
            }

            else {
                $query = $query->orderBy('id', 'desc');
            }



            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('limit') ?? 10);
        } else {
            $results = $query->get();
        }

        return
            response()->json(
                [
                    'error' => 0,
                    'page' => (request()->input('page')) ? request()->input('page') : 1,
                    'total' => count($query->get()),
                    'limit' => (request()->input('limit')) ? request()->input('limit') : 9,
                    'error' => 0,
                    'data' => $this->_config['resource']::collection($results)->forPage((isset($params['page']) ? $params['page'] : 1), (isset($params['limit']) ? $params['limit'] : 9)),
                ]);


        //} catch
        //(\Exception $x) {
        // return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        //}
    }

    /**
     * Returns a individual resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function get($id)
    {
        try {
            return response()->json(
                [
                    'error' => 0,
                    'data' => new $this->_config['resource'](
                        $this->repository->findOrFail($id))
                ]);


        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Delete's a individual resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        try {
            $wishlistProduct = $this->repository->findOrFail($id);

            $this->repository->delete($id);

            return response()->json([
                'error' => 0,
                'message' => 'Item removed successfully.'
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }
}
