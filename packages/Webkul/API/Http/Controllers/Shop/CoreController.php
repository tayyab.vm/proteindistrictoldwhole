<?php

namespace Webkul\API\Http\Controllers\Shop;

use Webkul\API\Http\Resources\Core\Error;

/**
 * Core controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CoreController extends Controller
{
    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getConfig()
    {
        try {
            $configValues = [];

            foreach (explode(',', request()->input('_config')) as $config) {
                $configValues[$config] = core()->getConfigData($config);
            }

            return response()->json([
                'error'=>0,
               'data' => $configValues
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getLine(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCountryStateGroup()
    {
        try {
            return response()->json([
                'error'=>0,
               'data' => core()->groupedStatesByCountries()
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function switchCurrency()
    {
        try {
            return response()->json([
                'error'=>0,
                'message'=>'Done'
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function switchLocale()
    {
        try {
            return response()->json([
                'error'=>0,
                'message'=>'Done'
            ]);
        } catch (\Exception $x) {
            return Error::JsonError($x->getCode(), $x->getCode(), 'faild', 'خطأ', 'Error');
        }
    }
}
