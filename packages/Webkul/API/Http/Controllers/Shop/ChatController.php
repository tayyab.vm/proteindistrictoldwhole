<?php

namespace Webkul\API\Http\Controllers\Shop;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Webkul\API\Http\Resources\Core\Error;
use Webkul\Customer\Models\chatModel as Chat;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';

        auth()->setDefaultDriver($this->guard);

        $this->middleware('auth:' . $this->guard, ['only' => ['get', 'update', 'destroy']]);

        $this->_config = request('_config');


        //$this->middleware('oauth', ['except' => ['index']]);
    }

    /**
     * @OA\Get(path="/api/v1/chat",
     *   tags={"Chat"},
     *   summary="Returns list of messages",
     *   description="Returns list of messages",
     *   operationId="index",
     *   security={{
     *     "bearerAuth":{}
     *   }},
     *   @OA\Parameter(
     *     name="imei",
     *     in="query",
     *     description="default IMEI",
     *     required=false,
     *     @OA\Schema(type="integer",format="int32")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Messages list",
     *   ),
     *  @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */
    public function index(Request $request)
    {
        try {
            $user = Auth::user();
            if ($user) {
                $chatt = Chat::
                where('from', '=', $user->id)->
                orWhere('to', '=', $user->id)->
                orderBy('id', 'desc');


                $message=$chatt->paginate();
                foreach ($chatt->where('from',0)->get() as $c)
                {
                    $c->seen=1;
                    $c->save();
                }


                return response()->json([
                    'error' => 0,
                    'data' => $message
                ]);
            } else
                return $message = Error::JsonError("100", "100", "Faild", "يجب تسجيل الدخول", "Please Register");
        } catch (\Exception $ex) {
            Error::JsonError($ex->getCode(), $ex->getCode(), "Faild", $ex->getMessage(), $ex->getMessage());
        }

    }
    /**
     * @OA\Post(path="/api/v1/chat",
     *   tags={"Chat"},
     *   summary="Send a new message",
     *   description="This can only be done by the logged in user.",
     *   operationId="store",
     *   security={{
     *     "bearerAuth":{}
     *   }},
     *  @OA\requestBody(
     *   required=false,
     *     description="Message sent.",
     *   content={
     *      @OA\MediaType(mediaType="multipart/form-datad",
     *      @OA\Schema(type="object",
     *
     *      @OA\Property(property="imei", type="integer",format="int32", description="default IMEI"),
     *
     *      @OA\Property(property="message", type="string"),
     *
     *      @OA\Property(property="attachment", type="string",format="binary"),
     *       ),
     *
     *      )},
     * ),
     *   @OA\Response(response="200", description="The message has been sent!"),
     *  @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */
    //application/x-www-form-urlencode
    public function store(Request $request)
    {
        try {

            $this->validateRequest($request);
            $from = null;
            $user = Auth::user();
            if ($user)
                $from = $user;
            else {
                return Error::JsonError("100", "100", "Faild", "يجب تسجيل الدخول", "Please Register");
            }
            $to = 0; //User::find($request->input('to'));


            if ($request->file('attachment')) {
                $picName = $request->file('attachment')->getClientOriginalName();
                $picName = uniqid() . '_' . $picName;
                $path = 'uploads' . DIRECTORY_SEPARATOR . 'chat_attachment';
                $destinationPath = rtrim(app()->basePath('public/' . $path), '/');//public_path($path); // upload path
                //dd($request);
                //File::makeDirectory($destinationPath, 0777, true, true);
                $request->file('attachment')->move($destinationPath, $picName);

                $message = Chat::create([
                        'from' => $from->id,
                        'to' => $to,
                        'message' => $request->input('message'),
                        'attachment' => $path . DIRECTORY_SEPARATOR . $picName,]
                /*$request->all()*/);
            } else {
                $message = Chat::create([
                    'from' => $from->id,
                    'to' => $to,
                    'message' => $request->input('message'),
                    'datetime' => date("Y-m-d H:i:s"),
                ]);
            }
            // $this->insertNotification(/*$source*/ $from->id, /*$destination*/ ($to) ? $to : 0, /*$type=محادثة جديدة*/ 13, /*$request*/ $message->id);

            return response()->json([
                'error' => 0,
                'data' => "The Message with id {$message->id}, has been sent!"
            ]);
        } catch (\Exception $ex) {
            Error::JsonError($ex->getCode(), $ex->getCode(), "Faild", $ex->getMessage(), $ex->getMessage());
        }


    }

    /**
     * @OA\Get(path="/api/v1/chat/{message}",
     *   tags={"Chat"},
     *   summary="Returns specific message with Id",
     *   description="Returns specific message with Id",
     *   operationId="show",
     *   security={{
     *     "bearerAuth":{}
     *   }},
     *   @OA\Parameter(
     *     name="message",
     *     in="path",
     *     description="Message Id",
     *     required=true,
     *     @OA\Schema(ref="/api/v1/chat/Chat")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Message object",
     *   ),
     *  @OA\Response(
     *         response=404,
     *         description="The message with specified id, doesn't exists.",
     *     ),
     *  @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */
    public function show($id)
    {
        try {
            $message = Chat::find($id);
            /*$patient = Patient::withTrashed()->find($id);
            if ($patient->trashed()) {
                $patient->restore();

            }
            */
            if ($message) {

                return response()->json([
                    'error' => 0,
                    'data' => $message
                ]);
            }

            return Error::JsonError("100", "100", "Faild", "يجب تسجيل الدخول", "Please Register");
        } catch (\Exception $ex) {
            Error::JsonError($ex->getCode(), $ex->getCode(), "Faild", $ex->getMessage(), $ex->getMessage());
        }
    }


    /**
     * @OA\Put(path="/api/v1/chat/{message}",
     *   tags={"Chat"},
     *   summary="Update message status",
     *   description="This can only be done by the logged in user.",
     *   operationId="update",
     *   security={{
     *     "bearerAuth":{}
     *   }},
     *   @OA\Parameter(
     *     name="message",
     *     in="path",
     *     description="Message Id",
     *     required=true,
     *     @OA\Schema(type="integer")
     *   ),
     *   @OA\Parameter(
     *     name="delivered",
     *     in="query",
     *     description="Change status to delivered",
     *     required=false,
     *     @OA\Schema(type="integer", format="int32", description=" 0 not delivered, 1 delivered")
     *   ),
     *   @OA\Parameter(
     *     name="seen",
     *     in="query",
     *     description="Change status to seen",
     *     required=false,
     *     @OA\Schema(type="integer", format="int32", description=" 0 not seen, 1 seen")
     *   ),
     *   @OA\Response(response="200", description="The request has been updated!"),
     *  @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     */
    //application/x-www-form-urlencode
    public function update($message, Request $request)
    {
        try {
            $user = Auth::user();
            $this->validateUpdateRequest($request);
            //dd($message);
            $chat = Chat::find($message);

            if ($chat && ($chat->from==$user->id||$chat->to==$user->id)) {
                if ($request->input('delivered'))
                    $chat->delivered = $request->input('delivered');

                if ($request->input('seen'))
                    $chat->seen = $request->input('seen');

                $chat->save();

                return response()->json([
                    'error' => 0,
                    'data' => "The Message with id {$chat->id}, has been Updated!"
                ]);
            }

            return Error::JsonError("100", "100", "Faild", "Fail", "Fail");
        } catch (\Exception $ex) {
            Error::JsonError($ex->getCode(), $ex->getCode(), "Faild", $ex->getMessage(), $ex->getMessage());
        }
    }

    /**
     * @OA\Delete(path="/api/v1/chat/{message}",
     *   tags={"Chat"},
     *   summary="Delete message",
     *   description="This can only be done by the logged in user.",
     *   operationId="destroy",
     *   security={{
     *     "bearerAuth":{}
     *   }},
     *   @OA\Parameter(
     *     name="message",
     *     in="path",
     *     description="The message Id that needs to be deleted",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(response=400, description="Invalid message Id supplied"),
     *   @OA\Response(response=404, description="Patient not found"),
     *   @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     * )
     */
    public function destroy($message_id)
    {
        try {
            $message = Chat::find($message_id);
            if ($message) {
                //$user->message()->detach();
                //$user->client()->detach();
                $message->delete();

                return response()->json([
                    'error' => 0,
                    'data' => "The Message with id {$message->id}, has been Deleted!",

                ]);
            }
            return Error::JsonError("422", "422", "Faild", "مدخلات خاطئة", "Unprocessable Entity");
        } catch (\Exception $ex) {
            Error::JsonError($ex->getCode(), $ex->getCode(), "Faild", $ex->getMessage(), $ex->getMessage());
        }
    }

    function validateRequest(Request $request, Chat $message = null)
    {

        $validator = Validator::make(request()->all(), [
            'imei' => 'numeric',
            //'to' => 'required|numeric',
            'message' => '',
            'attachment' => 'file|max:1024',
        ]);
        if ($validator->fails()) {
            return Error::JsonError(422, 422, 'Failed', 'البيانات غير صالحة', $validator->messages()->first());

        }
        return true;

    }

    function validateUpdateRequest(Request $request)
    {

        $validator = Validator::make(request()->all(), [
            'delivered' => 'numeric|in:0,1',
            'seen' => 'numeric|in:0,1'
        ]);
        if ($validator->fails()) {
            return Error::JsonError(422, 422, 'Failed', 'البيانات غير صالحة', $validator->messages()->first());
        }
        return true;
    }

}
