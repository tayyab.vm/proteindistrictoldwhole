<?php

namespace Webkul\Customer\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class chatModel extends Model
{

    protected $table = 'chat';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['message','from','to','attachment','id'];

    public static function countUnreadMessages($customers_id,$type=0)
    {
        $result = self::select('chat.id');



                $result = $result ->leftJoin('customers', 'customers.id', '=', 'chat.from');

                $result->whereNull('chat.deleted_at');
                $result->whereNotNull('customers.id');
                $result->where('chat.to', '0');





        $result->where('chat.seen',0);
        $result->whereNull('chat.deleted_at');
        $result = $result->get();

        return count($result);

    }
    public static function countUnreadMessages2($customers_id)
    {
        $result = self::select('chat.id');



            $result = $result ->leftJoin('customers', 'customers.id', '=', 'chat.from');

            $result->whereNull('chat.deleted_at');
            $result->whereNotNull('customers.id');

            $result->where('customers.id', $customers_id);



        $result->where('chat.seen',0);
        $result->whereNull('chat.deleted_at');
        $result = $result->get();

        return count($result);

    }
    public static function getAllMessages($customers_id = 0, $search = "")
    {
        $result=DB::table('chat as t1')->
        select('t1.*','customers.id as customers_id',DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as full_name'))
            ->leftJoin('chat AS t2', function($join)
            {
                $join->on('t2.from', '=', 't1.from');
                $join->on('t1.id','<','t2.id');

            })

            ->leftJoin('customers', 'customers.id', '=', 't1.from')

        ->whereNull('t2.from');


        if ($customers_id > 0)
        {

            $result = $result->where(function ($query) use ($customers_id) {
                $query->where('t1.from', $customers_id);
                $query->orwhere('t1.to', $customers_id);
            });
        }
        if ($search != "") {
            $result->where('t1.message', 'like', '%' . $search . '%');
        }

        $result->whereNull('t1.deleted_at')->where('t1.from','<>','0');
        $result = $result->orderby('t1.id', 'desc')->get();

        return $result;

    }

    public static function getMessages($customers_id,$search="")
    {
        $result = self::select('*');
        $result = $result->where(function ($query) use ($customers_id) {
            $query->where('chat.from', $customers_id);
            $query->orwhere('chat.to', $customers_id);
        });
        if($search!="")
        {
            $result->where('message','like','%'.$search.'%');
        }

        $result->whereNull('chat.deleted_at');
        $result = $result->orderby('id','asc')->get() ;

        return $result;

    }

    public static function getNewMessages($customers_id=0)
    {
        if($customers_id>0) {
            $result = self::select('*');
            $result = $result->where(function ($query) use ($customers_id) {
                $query->where('chat.from', $customers_id);
                $query->orwhere('chat.to', $customers_id);
            });
            $result->whereNull('chat.deleted_at');
            $result = $result->orderby('id','desc')->take(8)->get()->reverse() ;
        }
        else if($customers_id==0) {


            $result = self::select('*','customers.image as image','customers.id as customers_id',DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as full_name'))
            ->leftJoin('customers', 'customers.id', '=', 'chat.from');


            $result->where('chat.to','0');
            $result->whereNull('chat.deleted_at');
            $result->whereNotNull('customers.id');
            $result = $result->orderby('chat.id','desc')->distinct('chat.from')->get();
        }




        return $result;

    }




}
