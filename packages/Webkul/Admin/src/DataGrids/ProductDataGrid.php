<?php

namespace Webkul\Admin\DataGrids;

use DB;
use Webkul\Ui\DataGrid\DataGrid;

/**
 * ProductDataGrid Class
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductDataGrid extends DataGrid
{
    protected $sortOrder = 'desc'; //asc or desc

    protected $index = 'product_id';

    protected $itemsPerPage = 20;

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('product_flat')
            ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
            ->leftJoin('attribute_families', 'products.attribute_family_id', '=', 'attribute_families.id')
            ->leftJoin('dtype', 'products.dtype', '=', 'dtype.id')
            ->leftJoin('brand', 'products.brand', '=', 'brand.id')
            ->leftJoin('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
            ->select('product_flat.product_id as product_id', 'product_flat.sku as product_sku', 'product_flat.name as product_name', 'products.type as product_type', 'product_flat.status', 'product_flat.price', 'product_flat.minstock as minstock', 'attribute_families.name as attribute_family', DB::raw('SUM(product_inventories.qty) as quantity'), 'brand.name as product_brand', 'dtype.name as product_dtype')
            ->where('channel', core()->getCurrentChannelCode())
            ->where('locale', app()->getLocale())
            ->groupBy('product_flat.product_id');

        $this->addFilter('product_id', 'product_flat.product_id');
        $this->addFilter('product_name', 'product_flat.name');
        $this->addFilter('product_sku', 'product_flat.sku');
        $this->addFilter('status', 'product_flat.status');
        $this->addFilter('minstock', 'product_flat.minstock');
        $this->addFilter('product_brand', 'brand.name');
        $this->addFilter('product_dtype', 'dtype.name');
        $this->addFilter('product_type', 'products.type');

        $this->addFilter('attribute_family', 'attribute_families.name');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'product_id',
            'label' => trans('admin::app.datagrid.id'),
            'type' => 'number',
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },

            'searchable' => false,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'product_sku',
            'label' => trans('admin::app.datagrid.sku'),
            'type' => 'string',
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },
            'searchable' => true,
            'sortable' => true,

            'filterable' => true
        ]);


        $this->addColumn([
            'index' => 'product_name',
            'label' => trans('admin::app.datagrid.name'),
            'type' => 'string',
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },
            'searchable' => true,
            'sortable' => true,

            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'attribute_family',
            'label' => trans('admin::app.datagrid.attribute-family'),
            'type' => 'string',
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },
            'searchable' => true,
            'sortable' => true,

            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'product_type',
            'label' => trans('admin::app.datagrid.type'),
            'type' => 'string',
            'sortable' => true,
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },
            'searchable' => true,

            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'product_dtype',
            'label' => 'Disply Type',
            'type' => 'string',
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },
            'sortable' => true,
            'searchable' => true,

            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'product_brand',
            'label' => 'Brand',
            'type' => 'string',
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },
            'sortable' => true,

            'searchable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'status',
            'label' => trans('admin::app.datagrid.status'),
            'type' => 'boolean',
            'sortable' => true,
            'searchable' => false,
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },

            'filterable' => true,
            'wrapper' => function ($value) {
                if ($value->status == 1)
                    return 'Active';
                else
                    return 'Inactive';
            }
        ]);


        $this->addColumn([
            'index' => 'price',
            'label' => trans('admin::app.datagrid.price'),
            'type' => 'price',
            'sortable' => true,
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },

            'searchable' => false,
            'filterable' => true
        ]);
        $this->addColumn([
            'index' => 'minstock',
            'label' => 'Min Stock',
            'type' => 'number',

            'sortable' => true,
            'searchable' => true,
            'filterable' => true,
            'wrapper' => function ($value) {

                return $value->minstock;
            },
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },

        ]);

        $this->addColumn([
            'index' => 'quantity',
            'label' => trans('admin::app.datagrid.qty'),
            'type' => 'number',

            'sortable' => true,
            'searchable' => false,
            'filterable' => false,
            'warning' => function ($value) {
                if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                    return "1";
                else if ($value->quantity == 0)
                    return "2";
                else
                    return "0";
            },
            'wrapper' => function ($value) {
                if (is_null($value->quantity))
                    return 0;
                else
                    return $value->quantity;
            }

        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'type' => 'Edit',
            'method' => 'GET', // use GET request only for redirect purposes
            'route' => 'admin.catalog.products.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'method' => 'POST', // use GET request only for redirect purposes
            'route' => 'admin.catalog.products.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'product']),
            'icon' => 'icon trash-icon'
        ]);

        $this->enableAction = true;
        $x = (function ($value) {
            if (!is_null($value->minstock) && $value->minstock >= $value->quantity  && $value->quantity >0)
                return 1;
            else if ($value->quantity == 0)
                return 2;
            else
                return 0;
        });
        $this->warning = $x;

    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => 'Delete',
            'action' => route('admin.catalog.products.massdelete'),
            'method' => 'DELETE'
        ]);

        $this->addMassAction([
            'type' => 'update',
            'label' => 'Update Status',
            'action' => route('admin.catalog.products.massupdate'),
            'method' => 'PUT',
            'options' => [
                'Active' => 1,
                'Inactive' => 0
            ]
        ]);

        $this->enableMassAction = true;
    }
}