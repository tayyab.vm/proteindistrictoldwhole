@if ($categories->count())
<accordian :title="'{{ __('admin::app.catalog.products.categories') }}'" :active="true">
    <div slot="body">

        {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.controls.before', ['product' => $product]) !!}

        <tree-view behavior="normal" value-field="id" name-field="categories" input-type="checkbox" items='@json($categories)' value='@json($product->categories->pluck("id"))'></tree-view>
        <div class="control-group "   :class="[errors.has('goal') ? 'has-error' : '']">
            <label for="type" class="required">Goal</label>
            <select class="control select2-multiple" v-validate="'required'" id="goal" name="goal[]"  data-vv-as="&quot;goal&quot;" multiple="multiple">
                @foreach($goals as $goal)
                    <option value="{{ $goal->id }}" {{in_array($goal->id,explode(',',$product->goal))?'selected':''}}>{{ $goal->name }}</option>
                @endforeach
            </select>

            <span class="control-error" v-if="errors.has('goal[]')">@{{ errors.first('goal[]') }}</span>


        </div>
        <div class="control-group "  :class="[errors.has('dtype') ? 'has-error' : '']">
            <label for="type" class="hidden required">Display Type</label>
            <select class="control" v-validate="'required'" id="dtype" name="dtype"  data-vv-as="&quot;dtype&quot;">
                @foreach($dtypes as $dtype)
                    <option value="{{ $dtype->id }}" {{($product->dtype==$dtype->id)?'selected':''}}>{{ $dtype->name }} </option>
                @endforeach
            </select>

            <span class="control-error" v-if="errors.has('dtype')">@{{ errors.first('dtype') }}</span>
        </div>


        {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.controls.after', ['product' => $product]) !!}


    </div>
</accordian>
@endif