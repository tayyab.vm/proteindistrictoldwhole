@extends('admin::layouts.content')

@section('page_title')
   Goal
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Goals</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.goals.create') }}" class="btn btn-lg btn-primary">
                   Add Goals
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('goals','Webkul\Admin\DataGrids\GoalDataGrid')
            {!! $goals->render() !!}
        </div>
    </div>
@stop