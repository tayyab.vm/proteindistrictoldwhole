@extends('admin::layouts.content')

@section('page_title')
   Brand
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Brands</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.brands.create') }}" class="btn btn-lg btn-primary">
                   Add Brands
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('brands','Webkul\Admin\DataGrids\BrandDataGrid')
            {!! $brands->render() !!}
        </div>
    </div>
@stop