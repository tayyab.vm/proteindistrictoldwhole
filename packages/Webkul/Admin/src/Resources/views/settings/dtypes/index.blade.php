@extends('admin::layouts.content')

@section('page_title')
   Dtype
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Dtypes</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.dtypes.create') }}" class="btn btn-lg btn-primary">
                   Add Dtypes
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('dtypes','Webkul\Admin\DataGrids\DtypeDataGrid')
            {!! $dtypes->render() !!}
        </div>
    </div>
@stop