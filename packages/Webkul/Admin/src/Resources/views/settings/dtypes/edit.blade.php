@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.dtypes.edit-title') }}
@stop

@section('content')
    <div class="content">

        <form method="POST" action="{{ route('admin.dtypes.update', $dtype->id) }}" @submit.prevent="onSubmit">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Dtypes
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                     Update Dtypes
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <accordian :title="'Dtypes'" :active="true">
                        <div slot="body">

                            <div class="control-group" :class="[errors.has('code') ? 'has-error' : '']">
                                <label for="code" class="required">Code</label>
                                <input type="text" v-validate="'required'" class="control" id="code" name="code" data-vv-as="&quot;{{ __('admin::app.settings.dtypes.code') }}&quot;" value="{{ $dtype->code }}" disabled="disabled"/>
                                <input type="hidden" name="code" value="{{ $dtype->code }}"/>
                                <span class="control-error" v-if="errors.has('code')">@{{ errors.first('code') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="name" class="required">Name</label>
                                <input v-validate="'required'" class="control" id="name" name="name" data-vv-as="&quot;{{ __('admin::app.settings.dtypes.name') }}&quot;" value="{{ old('name') ?: $dtype->name }}"/>
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('name_ar') ? 'has-error' : '']">
                                <label for="name_ar" class="required">Name Ar</label>
                                <input v-validate="'required'" class="control" id="name" name="name_ar" data-vv-as="&quot;{{ __('admin::app.settings.dtypes.name')}}_ar&quot;" value="{{ old('name_ar') ?: $dtype->name_ar }}"/>
                                <span class="control-error" v-if="errors.has('name_ar')">@{{ errors.first('name_ar') }}</span>
                            </div>


                        </div>
                    </accordian>

                </div>
            </div>
        </form>
    </div>
@stop