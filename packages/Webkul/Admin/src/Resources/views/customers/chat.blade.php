@extends('admin::layouts.content')

@section('page_title')
    {{$data['title']}}
@stop


<link href="{{ asset('vendor/webkul/admin/assets/css/css/components-rounded.min.css')}}" rel="stylesheet"
      id="style_components" media="all" type="text/css"/>
<link href="{{ asset('vendor/webkul/admin/assets/css/css/font-awesome.css')}}" rel="stylesheet" id="style_components"
      media="all" type="text/css"/>
<link href="{{ asset('vendor/webkul/admin/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"
      id="style_components" media="all" type="text/css"/>
<link href="{{ asset('vendor/webkul/admin/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" media="all"
      type="text/css"/>
<link href="{{ asset('vendor/webkul/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{ asset('vendor/webkul/admin/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" media="all"
      type="text/css"/>
<link href="{{ asset('vendor/webkul/admin/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet"
      media="all" type="text/css"/>
<link href="{{ asset('vendor/webkul/admin/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" media="all"
      type="text/css"/>


@push('scripts')
    <script>
        $(document).ready(function () {
            var customer_id = $('#customer_id').val();
            fetch_user_chat_history(customer_id);
            $("#chat").stop().animate({scrollTop: $("#chat")[0].scrollHeight});

            setInterval(function () {
                var customer_id = $('#customer_id').val();
                fetch_user_chat_history(customer_id);
            }, 5000);
            $(document).on('click', '.showHistory', function () {
                $('.old-chat').load('../showallmessages/' + customer_id);
            });

            $(document).on('click', '.send_chat', function () {
                var customer_id = $('#customer_id').val();
                var from_id = $('#from_id').val();
                var to_id = $('#to_id').val();
                var chat_message = $('#chat_message').val();


                $.ajax({
                    url: "insert_chat",
                    method: "POST",
                    data: {from_id: from_id, to_id: to_id, chat_message: chat_message,_token: '{{csrf_token()}}'},
                    success: function (data) {
                        $('#chat_message').val('');
                        fetch_user_chat_history(customer_id);
                        $("#chat").stop().animate({scrollTop: $("#chat")[0].scrollHeight}, 10);
                    }
                })
            });
            $('#chat_message').on('keyup', function (e) {
                if (e.keyCode === 13) {
                    $('.send_chat').click();
                }
            });

            function fetch_user_chat_history(customer_id) {
                $('.chat_history').load('../fetch_user_chat_history/' + customer_id);
            }

            $(document).on('keyup', '#search', function () {
                var search = $('#search').val();
                var customer_id = $('#customer_id').val();
                if (search.length > 0) {

                    $('.old-chat').addClass("hidden");
                    $('.chat_history').addClass("hidden");
                    $('.chat_search').removeClass("hidden");
                    $('.chat_search').load(encodeURI('../showallmessages/' + customer_id + '?search=' + search));
                } else {
                    $('.old-chat').removeClass("hidden");
                    $('.chat_history').removeClass("hidden");
                    $('.chat_search').addClass("hidden");
                    $("#chat").stop().animate({scrollTop: $("#chat")[0].scrollHeight}, 10);
                }
            });

            function update_chat_history_data() {
                $('.chat_history').each(function () {
                    var to_user_id = $(this).data('touserid');
                    fetch_user_chat_history(to_user_id);
                });
            }

            $(document).on('click', '.ui-button-icon', function () {
                $('.user_dialog').dialog('destroy').remove();
            });

        });
    </script>
@endpush


@if(isset($date->success))
    <script>
        jQuery(document).ready(function () {
            toasterMessage('success', '{{ $date->success }}', 'Success Message');
        });

    </script>
@endif


@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>  {{$data['title']}}</h1>
            </div>
        </div>
        <div class="page-content">
                <div class="form-package">
                    <div class="row">
                        <div class="col-lg-12  col-xs-12 col-sm-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-hide hide"></i>
                                        <span class="caption-subject font-hide bold uppercase">Chats</span>
                                    </div>
                                    <div class="actions">
                                        <div class="portlet-input input-inline">
                                            <div class="input-icon right">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" id="customer_id" value="{{$data['customer']->id}}">
                                                <input type="hidden" id="from_id" value="{{0}}">
                                                <input type="hidden" id="to_id" value="{{$data['customer']->id}}">
                                                <input type="text" id="search" class="form-control input-circle"
                                                       placeholder="search..."></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body" id="chats">
                                    <div class="scroller " id="chat" style="height: 525px;" data-always-visible="1"
                                         data-rail-visible1="1">
                                        <div class="chat_search">
                                        </div>
                                        <div class="old-chat">
                                            <div class="showHistory btn btn-success btn-lg " style="width:100%">Show
                                                History
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="chat_history">

                                        </div>


                                    </div>
                                    <div class="chat-form">
                                        <div class="input-cont">
                                            <input class="form-control" id="chat_message" type="text"
                                                   placeholder="Type a message here..."/></div>
                                        <div class="btn-cont">
                                            <span class="arrow"> </span>
                                            <a href="javascript:;" class="btn blue icn-only send_chat ">
                                                <i class="fa fa-check icon-white"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
    </div>

@stop
