<ul class="chats">

    @foreach ($data['messages'] as $m)

        <li class="{{($m->from==0)?'out':'in'}}">
            @if($m->from==0)
                <img class="avatar" alt="" src="{{ asset('vendor/webkul/admin/assets/images/logo.png')}}" />
            @else
                <img class="avatar" alt="" src="{{ asset('vendor/webkul/admin/assets/images/avatar-img.png')}}" />
            @endif
            <div class="message">
                <span class="arrow"> </span>
                <a href="javascript:;" class="name"> {{($m->from==0)?'Protien District':\Webkul\Customer\Models\Customer::find($m->from)->first_name.' '.\Webkul\Customer\Models\Customer::find($m->from)->last_name}} </a>
                <span class="datetime"> at {{date('d-M-Y H:i',strtotime($m->datetime))}} </span>
                <span class="body"> {{$m->message}} </span>
                @if(!is_null($m->attachment))
                    <a href="{{$m->attachment}}"><h6>Download </h6></a>
                @endif
            </div>
        </li>

        @php
            $m->seen="1";
            $m->delivered="1";
            $m->save();
        @endphp
    @endforeach

</ul>
