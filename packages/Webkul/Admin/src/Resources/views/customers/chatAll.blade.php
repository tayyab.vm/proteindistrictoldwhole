@extends('admin::layouts.content')

@section('page_title')
     Chat Messages
@stop

@section('content')
    <div class="dashboard">
<div class="card" style="height: 700px !important;">
    <div class="card-title">
        Chat
    </div>

    <div class="card-info left">
        <ul>

            @foreach ($chat as $item)

                <li>
                    @if ($item->from)
                        <a href="{{ route('admin.customer.chat', $item->from) }}">
                            @endif

                            <div class="image">
                                <span class="icon profile-pic-icon"></span>
                            </div>

                            <div class="description">
                                <div class="name">
                                    {{ $item->full_name }}
                                    <span class="badge badge-success badge-sm">{{\Webkul\Customer\Models\chatModel::countUnreadMessages2($item->from)}}</span>
                                </div>
                                <span class="datetime"> at {{date('d-M-Y H:i',strtotime($item->datetime))}} </span>

                                <div class="info">

                                    {{$item->message}}
                                </div>
                            </div>

                            <span class="icon angle-right-icon"></span>

                            @if ($item->from)
                        </a>
                    @endif
                </li>

            @endforeach

        </ul>

    </div>
</div>
    </div>

@stop


