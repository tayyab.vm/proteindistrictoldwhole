<?php

namespace Webkul\Admin\Http\Controllers\Customer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Customer\Models\chatModel;
use Webkul\Customer\Repositories\CustomerRepository as Customer;
use Webkul\Customer\Repositories\CustomerGroupRepository as CustomerGroup;
use Webkul\Core\Repositories\ChannelRepository as Channel;

/**
 * Customer controlller
 *
 * @author    Rahul Shukla <rahulshukla.symfony517@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CustomerController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var array
     */
    protected $customer;

     /**
     * CustomerGroupRepository object
     *
     * @var array
     */
    protected $customerGroup;

     /**
     * ChannelRepository object
     *
     * @var array
     */
    protected $channel;

    /**
     * Create a new controller instance.
     *
     * @param \Webkul\Customer\Repositories\CustomerRepository $customer
     * @param \Webkul\Customer\Repositories\CustomerGroupRepository $customerGroup
     * @param \Webkul\Core\Repositories\ChannelRepository $channel
     */
    public function __construct(Customer $customer, CustomerGroup $customerGroup, Channel $channel)
    {


        $this->_config = request('_config');

        $this->middleware('admin');

        $this->customer = $customer;

        $this->customerGroup = $customerGroup;

        $this->channel = $channel;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view($this->_config['view']);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customerGroup = $this->customerGroup->findWhere([['code', '<>', 'guest']]);

        $channelName = $this->channel->all();

        return view($this->_config['view'], compact('customerGroup','channelName'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'channel_id' => 'required',
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'gender' => 'required',
            'email' => 'required|unique:customers,email',
            'date_of_birth' => 'date|before:today'
        ]);

        $data = request()->all();

        $password = bcrypt(rand(100000,10000000));

        $data['password'] = $password;

        $data['is_verified'] = 1;

        $this->customer->create($data);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Customer']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = $this->customer->findOrFail($id);

        $customerGroup = $this->customerGroup->findWhere([['code', '<>', 'guest']]);
        try {
            $suggestedCustomerGroup = $this->customerGroup->getModel()->where('points', '<=', $customer->points)->orderBy('points', 'desc')->get()->first()->name;
        }
        catch (\Exception $ex)
        {
            $suggestedCustomerGroup='guest';
        }

        $channelName = $this->channel->all();

        return view($this->_config['view'], compact('customer', 'customerGroup', 'channelName','suggestedCustomerGroup'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'channel_id' => 'required',
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'gender' => 'required',
            'email' => 'required|unique:customers,email,'. $id,
            'date_of_birth' => 'date|before:today'
        ]);

        $this->customer->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Customer']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = $this->customer->findorFail($id);

        try {
            $this->customer->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Customer']));

            return response()->json(['message' => true], 200);
        } catch(\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Customer']));
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * To load the note taking screen for the customers
     *
     * @return view
     */
    public function createNote($id)
    {
        $customer = $this->customer->find($id);

        return view($this->_config['view'])->with('customer', $customer);
    }

    /**
     * To store the response of the note in storage
     *
     * @return redirect
     */
    public function storeNote()
    {
        $this->validate(request(), [
            'notes' => 'string|nullable'
        ]);

        $customer = $this->customer->find(request()->input('_customer'));

        $noteTaken = $customer->update([
            'notes' => request()->input('notes')
        ]);

        if ($noteTaken) {
            session()->flash('success', 'Note taken');
        } else {
            session()->flash('error', 'Note cannot be taken');
        }

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To mass update the customer
     *
     * @return redirect
     */
    public function massUpdate()
    {
        $customerIds = explode(',', request()->input('indexes'));
        $updateOption = request()->input('update-options');

        foreach ($customerIds as $customerId) {
            $customer = $this->customer->find($customerId);

            $customer->update([
                'status' => $updateOption
            ]);
        }

        session()->flash('success', trans('admin::app.customers.customers.mass-update-success'));

        return redirect()->back();
    }

    /**
     * To mass delete the customer
     *
     * @return redirect
     */
    public function massDestroy()
    {
        $customerIds = explode(',', request()->input('indexes'));

        foreach ($customerIds as $customerId) {
            $this->customer->deleteWhere([
                'id' => $customerId
            ]);
        }

        session()->flash('success', trans('admin::app.customers.customers.mass-destroy-success'));

        return redirect()->back();
    }
    public function fetch_user_chat_history($id)
    {
        $customer = \Webkul\Customer\Models\Customer::find($id);

        if (!$customer)
            return redirect()->back();

        $data['title'] = "Chat with ".$customer->first_name." ".$customer->last_name;
        $data["customer"] = $customer;

        $data["messages"] = chatModel::getNewMessages($customer->id);


        return view('admin::customers.chatHistory')->with('data', $data);
    }
    public function insert_chat(Request $request)
    {
        if ($request->ajax()) {

                $from_id = 0;
                $to_id = $request->input("to_id");
                $chat_message = $request->input("chat_message");
                $chat = new chatModel();
                $chat->from = $from_id;
                $chat->to = $to_id;
                $chat->message = $chat_message;
                $chat->save();

            return response(['status' => true, 'message' => 'done'], 200);
        } else {
            return redirect()->back();
        }
    }
    public function showallmessages($id,Request $request)
    {
        $customer = \Webkul\Customer\Models\Customer::find($id);
        $search=$request->search;
        if (!$customer)
            return redirect("admin.customer.index");
        $data["customer"] = $customer;

        $data['title'] = "Chat with ".$customer->first_name." ".$customer->last_name;


        $data["messages"] = chatModel::getMessages($customer->id,$search);

        if(strlen($search)==0)
            return view('admin::customers.chatoldHistory')->with('data', $data);
        else
            return view('admin::customers.chatSearch')->with('data', $data);
    }

    public function chatAll()
    {
        $chat=chatModel::getAllMessages()->take(100);
        return view('admin::customers.chatAll')->with('chat', $chat);
    }
    public function chat($id)
    {

        $customer = \Webkul\Customer\Models\Customer::find($id);

        if (!$customer)
            return redirect("admin.customer.index");
        $data=[];
        $data['title'] = "Chat with ".$customer->first_name." ".$customer->last_name;
        $data["customer"] = $customer;



        if (\Session::has("success"))
          $data["success"] = \Session::get("success");

        if (\Session::has("error"))
            $data["error"] = \Session::get("error");
        $data["breadcrumbs"]["Chat"] = "";
        return view('admin::customers.chat')->with('data', $data);
    }
}