<tbody>
@if (count($records))
    @php $x=1;@endphp
    @foreach ($records as $key => $record)
        <tr>
            @if ($enableMassActions)
                <td @if(isset($column['warning'])&&$column['warning']($record)=="2") style="background-color:orangered; color:#fff; " @elseif(isset($column['warning'])&&$column['warning']($record)=="1")
                style="background-color: antiquewhite;" @endif>
            <span class="checkbox">
                            <input type="checkbox" v-model="dataIds" @change="select" value="{{ $record->{$index} }}">

                            <label class="checkbox-view" for="checkbox"></label>
                        </span>
                    
            </td>
            @endif

            @foreach ($columns as $column)
                @php
                    $columnIndex = explode('.', $column['index']);

                    $columnIndex = end($columnIndex);
                @endphp

                @if (isset($column['wrapper']))
                    @if (isset($column['closure']) && $column['closure'] == true)
                        <td @if(isset($column['warning'])&&$column['warning']($record)=="2") style="background-color:orangered; color:#fff; " @elseif(isset($column['warning'])&&$column['warning']($record)=="1")
                style="background-color: antiquewhite;"
                            @endif data-value="{{ $column['label'] }}">{!! $column['wrapper']($record) !!}</td>
                    @else
                        <td @if(isset($column['warning'])&&$column['warning']($record)=="2") style="background-color:orangered; color:#fff; " @elseif(isset($column['warning'])&&$column['warning']($record)=="1")
                style="background-color: antiquewhite;"
                            @endif data-value="{{ $column['label'] }}">{{ $column['wrapper']($record) }}</td>
                    @endif
                @else
                    @if ($column['type'] == 'price')
                        @if (isset($column['currencyCode']))
                            <td @if(isset($column['warning'])&&$column['warning']($record)=="2") style="background-color:orangered; color:#fff; " @elseif(isset($column['warning'])&&$column['warning']($record)=="1")
                style="background-color: antiquewhite;"
                                @endif data-value="{{ $column['label'] }}">{{ core()->formatPrice($record->{$columnIndex}, $column['currencyCode']) }}</td>
                        @else
                            <td @if(isset($column['warning'])&&$column['warning']($record)=="2") style="background-color:orangered; color:#fff; " @elseif(isset($column['warning'])&&$column['warning']($record)=="1")
                style="background-color: antiquewhite;"
                                @endif  data-value="{{ $column['label'] }}">{{ core()->formatBasePrice($record->{$columnIndex}) }}</td>
                        @endif
                    @else
                        <td @if(isset($column['warning'])&&$column['warning']($record)=="2") style="background-color:orangered; color:#fff; " @elseif(isset($column['warning'])&&$column['warning']($record)=="1")
                style="background-color: antiquewhite;"
                            @endif data-value="{{ $column['label'] }}">{{ $record->{$columnIndex} }}</td>
                    @endif
                @endif
            @endforeach

            @if ($enableActions)
                <td class="actions"
                    style="width: 100px; @if($warning&&$warning($record)=="1") background-color: antiquewhite; @elseif($warning&&$warning($record)=="2") background-color: orangered; color:#fff;  @endif"
                    data-value=" {{ __('ui::app.datagrid.actions') }}">
                    <div class="action">


                        @foreach ($actions as $action)
                            <a
                                    @if ($action['method'] == 'GET')
                                    href="{{ route($action['route'], $record->{$action['index'] ?? $index}) }}"
                                    @endif

                                    @if ($action['method'] != 'GET')
                                    v-on:click="doAction($event)"
                                    @endif

                                    data-method="{{ $action['method'] }}"
                                    data-action="{{ route($action['route'], $record->{$index}) }}"
                                    data-token="{{ csrf_token() }}"

                                    @if (isset($action['title']))
                                    title="{{ $action['title'] }}"
                                    @endif>
                                <span class="{{ $action['icon'] }}"></span>
                            </a>
                        @endforeach
                    </div>
                </td>
            @endif
        </tr>
        @php $x++;@endphp
    @endforeach
@else
    <tr>
        <td @if(isset($column['warning'])&&$column['warning']($record)=="2") style="background-color:orangered; color:#fff; " @elseif(isset($column['warning'])&&$column['warning']($record)=="1")
                style="background-color: antiquewhite;"
            @endif colspan="10" style="text-align: center;">{{ $norecords }}</td>
    </tr>
@endif
</tbody>
